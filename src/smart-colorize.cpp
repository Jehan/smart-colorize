/**
 *  @file smart-colorize.cpp
 *
 *  Copyright 2017 S�bastien Fourey & David Tchumperl�
 *
 *  This file is part "Smart-Colorize", a C++ implementation
 *  of the lineart semi-automatic colorization method.
 *
 *  Smart-Colorize is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Smart-Colorize is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Smart-Colorize.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
extern "C" {
#define GEGL_ITERATOR2_API
#include <gegl.h>

#include "../libgimpmath/gimpvector.h"
#include "gimplineart.h"
}
#include "CImg.h"
#include "LineArt_Tools.h"
using namespace cimg_library;

int
main (int   argc,
      char *argv[])
{
  SmartColorizeParams  params;
  GeglBuffer          *input_image;
  GeglBuffer          *color_spots = NULL;
  GeglBuffer          *color_layer = NULL;
  GeglBuffer          *_closed;
  GeglBuffer          *_closed2;
  GeglBuffer          *colorized_regions;
  GeglNode            *graph;
  GeglNode            *input;
  GeglNode            *sink;
  CImg<uchar>          inputImage;
  CImg<uchar>          colorSpots;
  CImg<uchar>          closed;
  CImg<uchar>          colorizedRegions;
  CImg<uchar>          overview;
  const char          *inputLineartName;
  const char          *inputColorSpotsName;
  const char          *outputColorizedRegionsName;
  const char          *outputClosedName;
  const char          *outputColorizedImageName;
  gboolean             stop_closed = FALSE;
  bool                 help;
  gint x, y;

  cimg_usage ("Apply the smart-colorize algorithm");

  inputLineartName = cimg_option ("-i", (char *)0, "Input lineart image file");
  inputColorSpotsName = cimg_option ("-c", (char *)0, "Input color spots image file");
  outputColorizedRegionsName = cimg_option ("-r", (char *)0, "Output colorized regions file");
  outputClosedName = cimg_option ("-x", (char *)0, "Closed strokes");
  outputColorizedImageName = cimg_option ("-o", (char *)0, "Output colorized image file");
  help = cimg_option ("-h", false, "Display this help");

  stop_closed = cimg_option ("-X", (char *)0, "Stop and show the closed strokes") ? TRUE : FALSE;
  params.splineMaxLength = (int) strtol (cimg_option ("--spline-max-len",
                                                      (char *) "60", "Spline max length"),
                                         NULL, 10);
  params.splineMaxAngle = strtof (cimg_option ("--spline-max-angle",
                                               (char *) "90.0", "Spline max angle (float)"),
                                  NULL);
  params.splineRoundness = strtof (cimg_option ("--spline-roundness",
                                                (char *) "1.0", "Spline roundness (float)"),
                                   NULL);
  params.allowSelfIntersections = cimg_option ("--allow-self-intersections", (char *) 0,
                                               "Allow self intersections") ? TRUE : FALSE;
  params.segmentsMaxLength = (int) strtol (cimg_option ("--segments-max-len",
                                                        (char *) "20", "Segments max length"),
                                           NULL, 10);
  params.contourDetectionLevel = strtof (cimg_option ("--contour-detection-level",
                                                      (char *) "0.92", "Contour detection level [0-1]"),
                                         NULL);
  params.normalEstimateMaskSize = (int) strtol (cimg_option ("--normal-estimate-mask-size",
                                                             (char *) "5", "Normal estimate mask size"),
                                                NULL, 10);
  params.erosion = (int) strtol (cimg_option ("--erosion",
                                              (char *) "-1", "Erosion (-1 for auto detection of the value)"),
                                 NULL, 10);
  params.endPointConnectivity = (int) strtol (cimg_option ("--end-point-connectivity",
                                                           (char *) "2", "End point connectivity"),
                                              NULL, 10);
  params.endPointRate = strtof (cimg_option ("--end-point-rate", (char *) "0.85", "End point rate [0-1]"), NULL);
  params.createdRegionsMinimumArea = (int) strtol (cimg_option ("--created-regions-minimum-area",
                                                                (char *) "100", "Created regions minimum area"),
                                                   NULL, 10);
  params.minimalLineArtArea = (int) strtol (cimg_option ("--minimal-lineart-area",
                                                         (char *) "5", "Minimal lineart area"),
                                            NULL, 10);
  params.createdRegionsSignificantArea = (int) strtol (cimg_option ("--created-regions-significant-area",
                                                                    (char *) "4", "Created region significant area"),
                                                       NULL, 10);
  params.smallSegmentsFromSplineSources = cimg_option ("--small-segments-from-spline-src", (char *) 0,
                                                       "Small segments from spline sources") ? TRUE : FALSE;

  if (help)
    {
      return 0;
    }
  if (!inputLineartName)
    {
      inputLineartName = "images/crop_inktober.png";
      inputColorSpotsName = "images/crop_inktober_spots.png";
    }

  gegl_init (&argc, &argv);
  graph = gegl_node_new ();
  input = gegl_node_new_child (graph,
                               "operation", "gegl:load",
                               "path", inputLineartName,
                               NULL);
  sink  = gegl_node_new_child (graph,
                               "operation", "gegl:buffer-sink",
                               "buffer", &input_image,
                               NULL);
  gegl_node_connect_to (input, "output",
                        sink, "input");
  gegl_node_process (sink);
  g_object_unref (graph);

  inputImage.load (inputLineartName);

  if (inputColorSpotsName)
    {
      colorSpots.load (inputColorSpotsName);

      graph = gegl_node_new ();
      input = gegl_node_new_child (graph,
                                   "operation", "gegl:load",
                                   "path", inputColorSpotsName,
                                   NULL);
      sink  = gegl_node_new_child (graph,
                                   "operation", "gegl:buffer-sink",
                                   "buffer", &color_spots,
                                   NULL);
      gegl_node_connect_to (input, "output",
                            sink, "input");
      gegl_node_process (sink);
      g_object_unref (graph);
    }

  // Gimp plug-in parameters
  //params.contourDetectionLevel = 0.95;
  //params.erosion = -1; // Erosion = -1 for auto detection of the value
  params.maximalColorSaturation = 32;
  params.minimalColorIntensity = 200;
  //params.endPointRate = 0.85;
  //params.endPointConnectivity = 2;
  //params.segmentsMaxLength = 20;
  //params.splineMaxAngle = 60;
  //params.splineRoundness = 1.0;
  //params.createdRegionsMinimumArea = 10 * 10;
  //params.allowSelfIntersections = true;

  //params.contourDetectionLevel = 0.92;
  //params.erosion = -1; // Erosion = -1 for auto detection of the value
  params.maximalColorSaturation = 32;
  params.minimalColorIntensity = 200;
  //params.endPointRate = 0.85;
  //params.endPointConnectivity = 2;
  //params.splineMaxLength = 60;
  //params.segmentsMaxLength = 20;
  //params.splineMaxAngle = 90;
  //params.splineRoundness = 1.0;
  //params.createdRegionsMinimumArea = 10 * 10;
  //params.allowSelfIntersections = true;

  // Extra parameters
  //params.minimalLineArtArea = 5;
  //params.normalEstimateMaskSize = 5;
  params.minimalRegionSize = 5;
  params.removeLargestLabel = true;
  //params.createdRegionsSignificantArea = 4;
  //params.smallSegmentsFromSplineSources = true;

  cimg::tic();
  _closed  = closeLineArt (inputImage, input_image, params, closed);
  _closed2 = gimp_lineart_close (input_image, FALSE,
                                 /* Renamed as stroke_threshold. */
                                 params.contourDetectionLevel,
                                 params.erosion,
                                 params.minimalLineArtArea,
                                 params.normalEstimateMaskSize,
                                 params.endPointRate,
                                 params.splineMaxLength,
                                 params.splineMaxAngle,
                                 params.endPointConnectivity,
                                 params.splineRoundness,
                                 params.allowSelfIntersections,
                                 params.createdRegionsSignificantArea,
                                 params.createdRegionsMinimumArea,
                                 params.smallSegmentsFromSplineSources,
                                 params.segmentsMaxLength);
    {
      gint diff = 0, count = 0;
      GeglBufferIterator   *gi;
      gi = gegl_buffer_iterator_new (_closed, NULL, 0, babl_format ("Y' u8"),
                                     GEGL_ACCESS_READ, GEGL_ABYSS_NONE, 2);
      gegl_buffer_iterator_add (gi, _closed2, NULL, 0, babl_format ("Y' u8"),
                                GEGL_ACCESS_READ, GEGL_ABYSS_NONE);
      while (gegl_buffer_iterator_next (gi))
        {
          guint8 *c1 = (guint8*) gi->items[0].data;
          guint8 *c2 = (guint8*) gi->items[1].data;
          gint    k;

          for (k = 0; k < gi->length; k++)
            {
              if (*c1 != *c2)
                diff++;
              count++;

              c1++;
              c2++;
            }
        }
      if (diff)
        printf("%d/%d pixels diff after closing line art\n", diff, count);
      g_object_unref (_closed);
      _closed = _closed2;
    }
  if (stop_closed)
    {
      cimg::toc();
      closed = convert_gegl_to_cimg<uchar> (_closed);
      goto end;
    }
  gimp_assert (color_spots &&
               compare_cimg_gegl_buffers (colorSpots, color_spots,
                                          "Comparing CImg and GEGL color spots"));
  if (colorSpots.is_empty ())
    {
      colorized_regions = autoColorLayer (closed, _closed, params, colorizedRegions, &color_layer);
    }
  else
    {
      colorLayerFromColorSpots (closed, _closed, colorSpots, color_spots, colorizedRegions);
    }
  cimg::toc();

  overview = superimposeGrayImage (inputImage, colorizedRegions);

end:
  if (outputColorizedImageName)
    {
      overview.save (outputColorizedImageName);
    }
  if (outputColorizedRegionsName)
    {
      colorizedRegions.save (outputColorizedRegionsName);
    }
  if (outputClosedName)
    {
      GeglNode *graph;
      GeglNode *input;
      GeglNode *sink;

      graph = gegl_node_new ();
      input = gegl_node_new_child (graph,
                                   "operation", "gegl:buffer-source",
                                   "buffer", _closed,
                                   NULL);
      sink  = gegl_node_new_child (graph,
                                   "operation", "gegl:save",
                                   "path", outputClosedName,
                                   NULL);
      gegl_node_connect_to (input, "output",
                            sink, "input");
      gegl_node_process (sink);
      g_object_unref (graph);
    }

  if (!outputColorizedImageName && !outputColorizedRegionsName)
    {
      if (stop_closed)
        {
          (inputImage, closed).display ();
        }
      else if (colorSpots.is_empty ())
        {
          (inputImage, colorizedRegions, overview).display ();
        }
      else
        {
          (inputImage, colorSpots, colorizedRegions, overview).display ();
        }
    }
  g_object_unref (input_image);
  g_object_unref (_closed);
  if (color_spots)
    g_object_unref (color_spots);
  if (color_layer)
    g_object_unref (color_layer);
  gegl_exit ();
}
