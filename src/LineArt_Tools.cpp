/**
 *  @file LineArt_Tools.cpp
 *
 *  Copyright 2017 S�bastien Fourey & David Tchumperl�
 *
 *  This file is part of "Smart-Colorize", a C++ implementation
 *  of the lineart semi-automatic colorization method.
 *
 *  Smart-Colorize is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Smart-Colorize is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Smart-Colorize.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
extern "C" {
#define GEGL_ITERATOR2_API
#include <gegl.h>
#include "../libgimpmath/gimpvector.h"
#include "gimplineart.h"
}
#include "LineArt_Tools.h"
using namespace cimg_library;

static bool significantAlphaChannel (GeglBuffer *buffer);
static void computeNormalsAndCurvature (const cimg_library::CImg<uchar> *mask,
                                        GeglBuffer                      *buffer,
                                        cimg_library::CImg<float>       *normals,
                                        cimg_library::CImg<float>       *curvatures,
                                        gfloat                        ***_normals,
                                        gfloat                         **_curvatures,
                                        int                              normalEstimateMaskSize);
static float cimg_medianDistanceMaximumPerConnectedComponent (const cimg_library::CImg<uchar> *mask);


static void  keep8ConnectedRegionsWithSignificantArea   (cimg_library::CImg<uchar> *image,
                                                         int                        size);
static int  cimg_numberOfTransitions                 (GArray                          *pixels,
                                                      const cimg_library::CImg<uchar> *image,
                                                      GeglBuffer                      *buffer,
                                                      gboolean                         borderValue);
/* TODO: remove after migration. */
static gboolean gimp_pixel_array_are_same            (GArray *set1,
                                                      GArray *set2);
static gboolean gimp_candidates_are_same             (GList  *list1,
                                                      GList  *list2);
static GList * cimg_findSplineCandidates    (GArray                          *maxPositions,
                                             const cimg_library::CImg<float> *normals,
                                             int                              distanceThreshold,
                                             float                            maxAngleDeg);
static bool cimg_curveCreatesOneSmallAndSignificantRegion (cimg_library::CImg<uchar> *mask,
                                                           GArray                    *points,
                                                           int                        lowerSizeLimit,
                                                           int                        upperSizeLimit);
/* END TODO. */

template<typename T>
cimg_library::CImg<T>
convert_gegl_to_cimg (const GeglBuffer *mask)
{
  CImg<T>  cimg_mask;
  GeglBuffer  *buffer = (GeglBuffer *) mask;

  cimg_mask.assign (gegl_buffer_get_width (buffer),
                    gegl_buffer_get_height (buffer));

  /*for (gint x = 0; x < gegl_buffer_get_width (buffer); x++)
    for (gint y = 0; y < gegl_buffer_get_height (buffer); y++)
      {
        guchar data;

        gegl_buffer_sample (buffer, x, y, NULL, &data, NULL,
                            GEGL_SAMPLER_NEAREST, GEGL_ABYSS_NONE);
        cimg_mask (x, y) = data;
      }*/
  gegl_buffer_get (buffer, NULL, 1.0, NULL, cimg_mask._data,
                   GEGL_AUTO_ROWSTRIDE, GEGL_ABYSS_NONE);
  gimp_assert (compare_cimg_gegl_buffers (cimg_mask, buffer,
                                          "Testing generated CImg image"));
  return cimg_mask;
}

template<typename T>
gboolean
compare_cimg_gegl_buffers (const cimg_library::CImg<T> &cimg_buffer,
                           GeglBuffer                  *gegl_buffer,
                           const gchar                 *info)
{
  gboolean same = TRUE;
  gint     gegl_bpp;
  gint     cimg_bpp;
  gint     diff_count = 0;
  gint     count = 0;

  printf ("** %s **\n", info);

  if (gegl_buffer_get_width (gegl_buffer) != cimg_buffer.width ()   ||
      gegl_buffer_get_height (gegl_buffer) != cimg_buffer.height () ||
      cimg_buffer.depth () != 1)
    {
      printf ("Buffer dimensions are different.\n");
      return FALSE;
    }

  gegl_bpp = babl_format_get_bytes_per_pixel (gegl_buffer_get_format (gegl_buffer));
  cimg_bpp = sizeof (T) * cimg_buffer._spectrum;

  if (gegl_bpp != cimg_bpp)
    {
      printf ("GEGL buffer is %d bpp - CImg buffer is %d bpp.\n",
              gegl_bpp, cimg_bpp);
      return FALSE;
    }

  for (gint i = 0; i < gegl_buffer_get_width (gegl_buffer); i++)
    for (gint j = 0; j < gegl_buffer_get_height (gegl_buffer); j++)
      {
        guchar gegl_pixel[gegl_bpp];

        gegl_buffer_sample (gegl_buffer, i, j, NULL, gegl_pixel, NULL,
                            GEGL_SAMPLER_NEAREST, GEGL_ABYSS_NONE);
        for (gint c = 0; c < cimg_buffer._spectrum; c++)
          {
            T *cimg_pixel = (T*) cimg_buffer.data (i, j, 0, c);

            count++;
            if (memcmp (gegl_pixel + c * sizeof (T), cimg_pixel, sizeof (T)))
              {
                guchar *data;

                printf ("Pixels (%d, %d), channel %d are different:",
                        i, j, c);

                printf ("\n\tCIMG data:");
                data = (guchar *) cimg_pixel;
                for (gint b = 0; b < sizeof (T); b++)
                  printf (" %x", data[b]);

                printf ("\n\tGEGL data:");
                data = (guchar *) gegl_pixel + c * sizeof (T);
                for (gint b = 0; b < sizeof (T); b++)
                  printf (" %x", data[b]);
                printf("\n");

                same = FALSE;
                diff_count++;
              }
          }
      }

  if (same)
    printf ("ALL GOOD!\n");
  else
    printf ("%d out of %d pixel data are different!\n",
            diff_count, count);

  return same;
}

GeglBuffer *
closeLineArt (const cimg_library::CImg<uchar> &lineArt,
              GeglBuffer                      *line_art,
              SmartColorizeParams              params,
              cimg_library::CImg<uchar>       &closed)
{
  //
  // Binarize lineart
  //
  GeglBuffer                *_closed;
  cimg_library::CImg<float>  gray = lineArt;
  cimg_library::CImg<uchar>  strokes;
  const Babl                *gray_format;
  GeglBuffer                *_strokes;
  GeglBufferIterator        *gi;
  GHashTable                *visited;
  GArray                    *keypoints;
  GArray                    *_keypoints;
  Pixel                     *point;
  GList                     *candidates;
  GList                     *_candidates;
  SplineCandidate           *candidate;
  gboolean                   has_significant_alpha_channel;
  guchar                     max_value = 0;
  gint                       width  = gegl_buffer_get_width (line_art);
  gint                       height = gegl_buffer_get_height (line_art);
  gint                       bpp;
  gint                       i;
  gfloat                  ***_normals    = g_new (gfloat **, width);
  gfloat                   **_curvatures = g_new (gfloat *, width);

  for (i = 0; i < width; i++)
    {
      gint j;

      _normals[i] = g_new (gfloat *, height);
      for (j = 0; j < height; j++)
        {
          _normals[i][j] = g_new (gfloat, 2);
          memset (_normals[i][j], 0, sizeof (gfloat) * 2);
        }
      _curvatures[i] = g_new (gfloat, height);
      memset (_curvatures[i], 0, sizeof (gfloat) * height);
    }

  compare_cimg_gegl_buffers (lineArt, line_art, "Comparing originals");

  has_significant_alpha_channel = cimg_significantAlphaChannel (gray, line_art);
  if (has_significant_alpha_channel)
    {
      /* Keep alpha channel as gray levels */
      gray_format = babl_format ("A u8");
      /* TODO: remove below. */
      gray.channel (gray.spectrum () - 1);
    }
  else
    {
      /* Keep luminance */
      gray_format = babl_format ("Y' u8");
      /* TODO: remove below. */
      if (gray.spectrum () == 1)
        {
          gray = gray.get_channel (0);
        }
      else
        {
          gray.sRGBtoRGB ();
          rgb2luminance (gray);
          gray.RGBtosRGB ();
        }
      // Negate
      float maxValue = G_MINFLOAT;
      for (float *p = (gray)._data, *_maxp = (gray)._data + (gray).size(); p < _maxp; ++p)
        {
          if (*p > maxValue)
            {
              maxValue = *p;
            }
        }
      for (float *p = (gray)._data, *_maxp = (gray)._data + (gray).size(); p<_maxp; ++p)
        { *p = maxValue - *p; }
    }
  /* TODO: remove below. */
  strokes = gray.get_threshold (255.0f * (1.0f - params.contourDetectionLevel), false, true /*strict*/);

  /* Transform the line art from any format to gray. */
  _strokes = gegl_buffer_new (gegl_buffer_get_extent (line_art),
                              gray_format);
  gegl_buffer_copy (line_art, NULL, GEGL_ABYSS_NONE, _strokes, NULL);

  bpp = babl_format_get_bytes_per_pixel (gray_format);
  if (! has_significant_alpha_channel)
    {
      /* Compute the bigger value */
      gi = gegl_buffer_iterator_new (_strokes, NULL, 0, NULL,
                                     GEGL_ACCESS_READ, GEGL_ABYSS_NONE, 1);
      while (gegl_buffer_iterator_next (gi))
        {
          guchar *data = (guchar*) gi->items[0].data;
          gint    k;

          for (k = 0; k < gi->length; k++)
            {
              if (*data > max_value)
                max_value = *data;
              data += bpp;
            }
        }
    }
  /* Make the image full black and white. */
  gi = gegl_buffer_iterator_new (_strokes, NULL, 0, NULL,
                                 GEGL_ACCESS_READWRITE, GEGL_ABYSS_NONE, 1);
  while (gegl_buffer_iterator_next (gi))
    {
      guchar *data = (guchar*) gi->items[0].data;
      gint    k;

      for (k = 0; k < gi->length; k++)
        {
          if (! has_significant_alpha_channel)
            /* Negate the value. */
            *data = max_value - *data;
          /* Apply a threshold. */
          if (*data > (guchar) (255.0f * (1.0f - params.contourDetectionLevel)))
            *data = 1;
          else
            *data = 0;
          data += bpp;
        }
    }

  compare_cimg_gegl_buffers (strokes, _strokes, "Comparing after B&W");

  if (params.erosion > 0)
    {
      printf("Erosion run with value: %d\n", params.erosion);
      strokes.erode (params.erosion);
      gimp_lineart_erode (_strokes, params.erosion);
    }
  else if (params.erosion == -1)
    {
      const float estimatedStrokeWidth = 2 * cimg_medianDistanceMaximumPerConnectedComponent (&strokes);
      const int   erodeSize = (int) roundf (estimatedStrokeWidth / 5);

      gimp_assert (estimatedStrokeWidth -
                   gimp_lineart_estimate_stroke_width (_strokes) < 0.00001);
      printf("Auto erosion (-1)\n");
      if (erodeSize)
        {
          printf("Erosion run with actual value: %d\n", 2 * erodeSize);
          strokes.erode (2 * erodeSize);
          gimp_lineart_erode (_strokes, 2 *erodeSize);
        }
    }
  compare_cimg_gegl_buffers (strokes, _strokes, "Comparing after erosion");

  // Denoise (remove small connected components)
  keep8ConnectedRegionsWithSignificantArea (&strokes, params.minimalLineArtArea);
  gimp_lineart_denoise (_strokes, params.minimalLineArtArea);
  if (! compare_cimg_gegl_buffers (strokes, _strokes, "Comparing after Denoising"))
    {
      strokes = convert_gegl_to_cimg<uchar> (_strokes);
      compare_cimg_gegl_buffers (strokes, _strokes, "Comparing after Denoising v2");
    }

  // Estimate normals & curvature
  CImg<float> normals (gegl_buffer_get_width (_strokes), gegl_buffer_get_height (_strokes), 2, 1);
  CImg<float> curvatures (gegl_buffer_get_width (_strokes), gegl_buffer_get_height (_strokes));
  computeNormalsAndCurvature (&strokes, _strokes, &normals, &curvatures,
                              _normals, _curvatures, params.normalEstimateMaskSize);
    {
      /* Temp test to compare with compute_normals_curvatures () */
      gfloat ***normals2    = g_new (gfloat **, width);
      gfloat  **curvatures2 = g_new (gfloat *, width);
      for (i = 0; i < width; i++)
        {
          gint j;

          normals2[i] = g_new (gfloat *, height);
          for (j = 0; j < height; j++)
            {
              normals2[i][j] = g_new (gfloat, 2);
              memset (normals2[i][j], 0, sizeof (gfloat) * 2);
            }
          curvatures2[i] = g_new (gfloat, height);
          memset (curvatures2[i], 0, sizeof (gfloat) * height);
        }

      gimp_lineart_compute_normals_curvatures (_strokes, normals2, curvatures2,
                                               params.normalEstimateMaskSize);
      for (i = 0; i < width; i++)
        {
          gint j;
          for (j = 0; j < height; j++)
            {
              gimp_assert (normals2[i][j][0] == _normals[i][j][0] &&
                           normals2[i][j][1] == _normals[i][j][1]);
              gimp_assert (curvatures2[i][j] == _curvatures[i][j]);
            }
        }
      for (i = 0; i < width; i++)
        {
          gint j;

          for (j = 0; j < height; j++)
            {
              g_free (normals2[i][j]);
            }
          g_free (normals2[i]);
          g_free (curvatures2[i]);
        }
      g_free (normals2);
      g_free (curvatures2);
    }
  /* Testing identical normals. */
  for (i = 0; i < width; i++)
    {
      gint j;
      for (j = 0; j < height; j++)
        {
          gimp_assert (normals (i, j, 0) == _normals[i][j][0] &&
                       normals (i, j, 1) == _normals[i][j][1]);
          gimp_assert (curvatures (i, j) == _curvatures[i][j]);
        }
    }

  curvatures.threshold (1.0f - params.endPointRate, true /* soft */, false /* not strict */);


  gfloat threshold = 1.0f - params.endPointRate;
  /*for (gfloat *ptrd = (*this)._data + (*this).size() - 1; ptrd>=(*this)._data; --ptrd)
    {
      const gfloat v = *ptrd;

      *ptrd = v >= threshold ? v - threshold :
                               (v <= - threshold ? v + threshold : 0.0f);
    }*/
  for (i = 0; i < width; i++)
    {
      gint j;
      for (j = 0; j < height; j++)
        {
          gfloat v = _curvatures[i][j];

          _curvatures[i][j] = v >= threshold ? v - threshold :
                                               (v <= -threshold ? v + threshold : 0.0f);
        }
    }

  keypoints = cimg_curvatureExtremums (curvatures);
  _keypoints = gimp_lineart_curvature_extremums (_curvatures, width, height);
  if (! gimp_pixel_array_are_same (keypoints, _keypoints))
    printf("NOOO! curvature extremums are different!\n");
  else
    printf("Curvature extremums are all GOOD!\n");

  //  CImg<uchar> kp;
  //  kp.assign (strokes.width (), strokes.height (), 1, 1, 0);
  //  for (const Pixel & p : keypoints)
  // {
  //    kp (p.x, p.y) = 255;
  //  }
  //  kp.display ();

  candidates = cimg_findSplineCandidates (keypoints, &normals,
                                          params.splineMaxLength,
                                          params.splineMaxAngle);
  _candidates = gimp_lineart_find_spline_candidates (_keypoints, _normals,
                                                     params.splineMaxLength,
                                                     params.splineMaxAngle);
  if (! gimp_candidates_are_same (candidates, _candidates))
    printf("NOOO! candidates are different!\n");
  else
    printf("Candidates are all GOOD!\n");
  closed = strokes;
  _closed = gegl_buffer_dup (_strokes);

  // Draw splines
  visited = g_hash_table_new_full ((GHashFunc) visited_hash_fun,
                                   (GEqualFunc) visited_equal_fun,
                                   (GDestroyNotify) g_free, NULL);
  while (_candidates)
    {
      Pixel    *p1 = g_new (Pixel, 1);
      Pixel    *p2 = g_new (Pixel, 1);
      gboolean  inserted = FALSE;

      candidate = (SplineCandidate *) _candidates->data;
      p1->x = candidate->p1.x;
      p1->y = candidate->p1.y;
      p2->x = candidate->p2.x;
      p2->y = candidate->p2.y;

      g_free (candidate);
      _candidates = g_list_delete_link (_candidates, _candidates);

      if ((! g_hash_table_contains (visited, p1) ||
           GPOINTER_TO_INT (g_hash_table_lookup (visited, p1)) < params.endPointConnectivity) &&
          (! g_hash_table_contains (visited, p2) ||
           GPOINTER_TO_INT (g_hash_table_lookup (visited, p2)) < params.endPointConnectivity))
        {
          float        distance = gimp_vector2_length_val (gimp_vector2_sub_val (*p1, *p2));
          GimpVector2  vect1 = pair2normal (*p1, _normals);
          GimpVector2  vect2 = pair2normal (*p2, _normals);
          GArray      *discreteCurve;

          gimp_vector2_mul (&vect1, distance);
          gimp_vector2_mul (&vect1, params.splineRoundness);
          gimp_vector2_mul (&vect2, distance);
          gimp_vector2_mul (&vect2, params.splineRoundness);

          discreteCurve = gimp_lineart_discrete_spline (*p1, vect1, *p2, vect2);

          const int transitions = params.allowSelfIntersections ?
                                    cimg_numberOfTransitions (discreteCurve, &strokes, _strokes, false) :
                                    cimg_numberOfTransitions (discreteCurve, &closed, _closed, false);

          gimp_assert (gimp_number_of_transitions (discreteCurve, _strokes, FALSE) ==
                       cimg_numberOfTransitions (discreteCurve, &strokes, _strokes, false) &&
                       gimp_number_of_transitions (discreteCurve, _closed, FALSE) ==
                       cimg_numberOfTransitions (discreteCurve, &closed, _closed, false));
          if (transitions == 2 &&
              ! cimg_curveCreatesOneSmallAndSignificantRegion (&closed, discreteCurve,
                                                               params.createdRegionsSignificantArea,
                                                               params.createdRegionsMinimumArea - 1))
            {
              gimp_assert (! gimp_lineart_curve_creates_region (_closed, discreteCurve,
                                                                params.createdRegionsSignificantArea,
                                                                params.createdRegionsMinimumArea - 1));
              for (i = 0; i < discreteCurve->len; i++)
                {
                  Pixel p = g_array_index (discreteCurve, Pixel, i);
                  gboolean set_cimg = FALSE;
                  gboolean set_gegl = FALSE;

                  if (closed.containsXYZC (p.x, p.y))
                    {
                      closed (p.x, p.y) = 1;
                      set_cimg = TRUE;
                    }
                  if (p.x >= 0 && p.x < gegl_buffer_get_width (_closed) &&
                      p.y >= 0 && p.y < gegl_buffer_get_height (_closed))
                    {
                      guchar val = 1;

                      gegl_buffer_set (_closed, GEGL_RECTANGLE ((gint) p.x, (gint) p.y, 1, 1), 0,
                                       NULL, &val, GEGL_AUTO_ROWSTRIDE);
                      set_gegl = TRUE;
                    }
                  gimp_assert (set_cimg == set_gegl);
                }
              g_hash_table_replace (visited, p1,
                                    GINT_TO_POINTER (GPOINTER_TO_INT (g_hash_table_lookup (visited, p1)) + 1));
              g_hash_table_replace (visited, p2,
                                    GINT_TO_POINTER (GPOINTER_TO_INT (g_hash_table_lookup (visited, p2)) + 1));
              inserted = TRUE;
            }
          else
            gimp_assert (transitions != 2 ||
                         gimp_lineart_curve_creates_region (_closed, discreteCurve,
                                                            params.createdRegionsSignificantArea,
                                                            params.createdRegionsMinimumArea - 1));
          g_array_free (discreteCurve, TRUE);
        }
      if (! inserted)
        {
          g_free (p1);
          g_free (p2);
        }
    }

  // Draw straight line segments
  point = (Pixel *) _keypoints->data;
  for (i = 0; i < _keypoints->len; i++)
    {
      Pixel    *p = g_new (Pixel, 1);
      gboolean  inserted = FALSE;

      *p = *point;

      if (! g_hash_table_contains (visited, p) ||
          (params.smallSegmentsFromSplineSources &&
           GPOINTER_TO_INT (g_hash_table_lookup (visited, p)) < params.endPointConnectivity))
        {
          GArray *segment = cimg_lineSegmentUntilHit (closed, *point,
                                                 pair2normal (*point, _normals),
                                                 params.segmentsMaxLength);
          GArray *_segment = gimp_lineart_line_segment_until_hit (_closed, *point,
                                                                  pair2normal (*point, _normals),
                                                                  params.segmentsMaxLength);
          gimp_assert (gimp_pixel_array_are_same (segment, _segment));

          if (segment->len &&
              ! cimg_curveCreatesOneSmallAndSignificantRegion (&closed, segment,
                                                               params.createdRegionsSignificantArea,
                                                               params.createdRegionsMinimumArea - 1))
            {
              gint j;

              gimp_assert (! gimp_lineart_curve_creates_region (_closed, segment,
                                                                params.createdRegionsSignificantArea,
                                                                params.createdRegionsMinimumArea - 1));
              for (j = 0; j < segment->len; j++)
                {
                  Pixel  p2 = g_array_index (segment, Pixel, j);
                  guchar val = 1;

                  closed (p2.x, p2.y) = 1;

                  gegl_buffer_set (_closed, GEGL_RECTANGLE ((gint) p2.x, (gint) p2.y, 1, 1), 0,
                                   NULL, &val, GEGL_AUTO_ROWSTRIDE);
                }
              g_hash_table_replace (visited, p,
                                    GINT_TO_POINTER (GPOINTER_TO_INT (g_hash_table_lookup (visited, p)) + 1));
              inserted = TRUE;
            }
          else
            gimp_assert (! segment->len || gimp_lineart_curve_creates_region (_closed, segment,
                                                                              params.createdRegionsSignificantArea,
                                                                              params.createdRegionsMinimumArea - 1));
          g_array_free (segment, TRUE);
        }
      if (! inserted)
        {
          g_free (p);
        }
      point++;
    }
  g_array_free (keypoints, TRUE);
  g_array_free (_keypoints, TRUE);
  g_object_unref (_strokes);
  for (i = 0; i < width; i++)
    {
      gint j;

      for (j = 0; j < height; j++)
        {
          g_free (_normals[i][j]);
        }
      g_free (_normals[i]);
      g_free (_curvatures[i]);
    }
  g_free (_normals);
  g_free (_curvatures);
  compare_cimg_gegl_buffers (closed, _closed, "Comparing returned value");

  return _closed;
}

GeglBuffer *
gimp_get_map (GeglBuffer *labels,
              gdouble    *colormap,
              gint        cmap_size,
              gint        n_channels)
{
  GeglBufferIterator *gi;
  GeglBuffer         *res;

  // TODO: make it u32!
  res = gegl_buffer_new (gegl_buffer_get_extent (labels),
                         babl_format ("RGB u8"));

  gi = gegl_buffer_iterator_new (labels, NULL, 0, babl_format ("Y u32"),
                                 GEGL_ACCESS_READ, GEGL_ABYSS_NONE, 2);
  gegl_buffer_iterator_add (gi, res, NULL, 0, babl_format ("RGB u8"),
                            GEGL_ACCESS_WRITE, GEGL_ABYSS_NONE);
  while (gegl_buffer_iterator_next (gi))
    {
      guint32 *in = (guint32*) gi->items[0].data;
      guint8 *out = (guint8*) gi->items[1].data;
      gint     k;

      for (k = 0; k < gi->length; k++)
        {
          if (*in < cmap_size)
            {
              out[0] = colormap[(*in) * n_channels];
              out[1] = colormap[(*in) * n_channels + 1];
              out[2] = colormap[(*in) * n_channels + 2];
            }
          else
            {
              out[0] = 0.0;
              out[1] = 0.0;
              out[2] = 0.0;
            }
          in++;
          out += 3;
        }
    }

  return res;
}

GeglBuffer *
autoColorLayer (const cimg_library::CImg<uchar> &lineArt,
                GeglBuffer                      *line_art,
                SmartColorizeParams              params,
                cimg_library::CImg<uchar>       &colorLayer,
                GeglBuffer                     **color_layer)
{
  GeglBufferIterator *gi;
  GeglBuffer         *color_buffer;
  GeglBuffer         *_labels;
  GeglBuffer         *distmap;
  GeglBuffer         *tmp;

  GeglNode           *graph;
  GeglNode           *input;
  GeglNode           *aux;
  GeglNode           *op;
  GeglNode           *sink;

  CImg<ulong>  labels = lineArt.get_label (false);
  // Set label 0 to a new value, and lineart strokes label to 0
  ulong        maxLabel = labels.max () + 1;

  _labels = gimp_get_labels (line_art, FALSE);
  //compare_cimg_gegl_buffers (labels, _labels, "Comparing CImg and GEGL labels");
  /* CImg saves on 64-bit. Let's save on 32-bit on our side.
   * So I compare with different bpp. */
    {
      gint diff_count = 0;

      for (gint i = 0; i < gegl_buffer_get_width (_labels); i++)
        {
          for (gint j = 0; j < gegl_buffer_get_height (_labels); j++)
            {
              guint32 gegl_label;
              ulong   cimg_label = labels (i, j);

              gegl_buffer_sample (_labels, i, j, NULL, &gegl_label, NULL,
                                  GEGL_SAMPLER_NEAREST, GEGL_ABYSS_NONE);

              if (gegl_label != (guint32) cimg_label)
                {
                  printf ("Labels (%d, %d) are different (%d vs %d)!\n",
                          i, j, gegl_label, cimg_label);

                  diff_count++;
                }
            }
        }
      if (diff_count)
        printf("%d labels different in GEGL\n", diff_count);
      else
        printf("Labels same in GEGL\n");
      gimp_assert (diff_count == 0);
    }

  for (int y = 0; y < gegl_buffer_get_height (_labels); ++y)
    for (int x = 0; x < gegl_buffer_get_width (_labels); ++x)
      {
        guchar val;

        gegl_buffer_sample (line_art, x, y, NULL, &val,
                            NULL, GEGL_SAMPLER_NEAREST, GEGL_ABYSS_NONE);
        if (lineArt (x, y))
          {
            gimp_assert (val == 1);
            labels (x, y) = 0;

            val = 0;
            gegl_buffer_set (_labels, GEGL_RECTANGLE (x, y, 1, 1), 0,
                             NULL, &val, GEGL_AUTO_ROWSTRIDE);
          }
        else if (!labels (x, y))
          {
            guint32 label = maxLabel;

            gegl_buffer_sample (_labels, x, y, NULL, &label,
                                NULL, GEGL_SAMPLER_NEAREST, GEGL_ABYSS_NONE);
            gimp_assert (val == 0 && label == 0);
            label = maxLabel;

            labels (x, y) = maxLabel;

            gegl_buffer_set (_labels, GEGL_RECTANGLE (x, y, 1, 1), 0,
                             NULL, &label, GEGL_AUTO_ROWSTRIDE);
          }
      }
  // COMPARE AGAIN!
    {
      gint diff_count = 0;

      for (gint i = 0; i < gegl_buffer_get_width (_labels); i++)
        {
          for (gint j = 0; j < gegl_buffer_get_height (_labels); j++)
            {
              guint32 gegl_label;
              ulong   cimg_label = labels (i, j);

              gegl_buffer_sample (_labels, i, j, NULL, &gegl_label, NULL,
                                  GEGL_SAMPLER_NEAREST, GEGL_ABYSS_NONE);

              if (gegl_label != (guint32) cimg_label)
                {
                  printf ("Labels (%d, %d) are different (%d vs %d)!\n",
                          i, j, gegl_label, cimg_label);

                  diff_count++;
                }
            }
        }
      if (diff_count)
        printf("%d labels different in GEGL\n", diff_count);
      else
        printf("Labels same in GEGL\n");
      gimp_assert (diff_count == 0);
    }
  cimg_removeSmallLabeledRegions (labels, params.minimalRegionSize);
  removeSmallLabeledRegions (_labels, params.minimalRegionSize);
  // COMPARE AGAIN!
    {
      gint diff_count = 0;

      for (gint i = 0; i < gegl_buffer_get_width (_labels); i++)
        {
          for (gint j = 0; j < gegl_buffer_get_height (_labels); j++)
            {
              guint32 gegl_label;
              ulong   cimg_label = labels (i, j);

              gegl_buffer_sample (_labels, i, j, NULL, &gegl_label, NULL,
                                  GEGL_SAMPLER_NEAREST, GEGL_ABYSS_NONE);

              if (gegl_label != (guint32) cimg_label)
                {
                  printf ("Labels (%d, %d) are different (%d vs %d)!\n",
                          i, j, gegl_label, cimg_label);

                  diff_count++;
                }
            }
        }
      if (diff_count)
        printf("%d labels different in GEGL\n", diff_count);
      else
        printf("Labels same in GEGL\n");
      gimp_assert (diff_count == 0);
    }

  /* Compute a distance map for the line art. */
  graph = gegl_node_new ();
  input = gegl_node_new_child (graph,
                               "operation", "gegl:buffer-source",
                               "buffer", line_art,
                               NULL);
  op  = gegl_node_new_child (graph,
                             "operation", "gegl:distance-transform",
                             "metric", GEGL_DISTANCE_METRIC_EUCLIDEAN,
                             "normalize", FALSE,
                             NULL);
  sink  = gegl_node_new_child (graph,
                               "operation", "gegl:buffer-sink",
                               "buffer", &distmap,
                               NULL);
  gegl_node_connect_to (input, "output",
                        op, "input");
  gegl_node_connect_to (op, "output",
                        sink, "input");
  gegl_node_process (sink);
  g_object_unref (graph);

  /* gegl:distance-transform returns distances as float. I want them as
   * unsigned ints without converting (i.e. not assuming pixel values).
   * Let's just loop through the map).
   */
  tmp = gegl_buffer_new (gegl_buffer_get_extent (distmap),
                         babl_format ("Y u8"));
  gi = gegl_buffer_iterator_new (distmap, NULL, 0, NULL,
                                 GEGL_ACCESS_READ, GEGL_ABYSS_NONE, 2);
  gegl_buffer_iterator_add (gi, tmp, NULL, 0, NULL,
                            GEGL_ACCESS_WRITE, GEGL_ABYSS_NONE);
  while (gegl_buffer_iterator_next (gi))
    {
      float  *data = (float*) gi->items[0].data;
      guint8 *out = (guint8*) gi->items[1].data;
      gint    k;

      for (k = 0; k < gi->length; k++)
        {
          *out = (guint8) MIN (round (*data), G_MAXUINT8);
          data++;
          out++;
        }
    }
  g_object_unref (distmap);
  distmap = tmp;

  /* Compare distance map to CImg computed one. TODO: remove! */
  CImg<float> ld = (lineArt.get_distance (0) *= -1.0);
    {
      gint diff_count = 0;
      gint count = 0;

      for (gint i = 0; i < gegl_buffer_get_width (distmap); i++)
        {
          for (gint j = 0; j < gegl_buffer_get_height (distmap); j++)
            {
              guint8 gegl_dist;
              float cimg_dist = ld (i, j);

              gegl_buffer_sample (distmap, i, j, NULL, &gegl_dist, NULL,
                                  GEGL_SAMPLER_NEAREST, GEGL_ABYSS_NONE);

              if (ABS (gegl_dist + round (cimg_dist)) > 0.000001)
                diff_count++;
              count++;
            }
        }
      gimp_assert (diff_count == 0);
    }

  /* Expected input label map needs an alpha channel, whose purpose is only to
   * flag unlabelled pixels (alpha == 0).
   */
  tmp = gegl_buffer_new (gegl_buffer_get_extent (distmap),
                         babl_format ("YA u32"));
  gi = gegl_buffer_iterator_new (_labels, NULL, 0, NULL,
                                 GEGL_ACCESS_READ, GEGL_ABYSS_NONE, 2);
  gegl_buffer_iterator_add (gi, tmp, NULL, 0, NULL,
                            GEGL_ACCESS_WRITE, GEGL_ABYSS_NONE);
  while (gegl_buffer_iterator_next (gi))
    {
      guint32 *data = (guint32*) gi->items[0].data;
      guint32 *out  = (guint32*) gi->items[1].data;
      gint     k;

      for (k = 0; k < gi->length; k++)
        {
          out[0] = *data;
          if (*data == 0)
            out[1] = 0;
          else
            out[1] = 1;
          data++;
          out += 2;
        }
    }
  g_object_unref (_labels);
  _labels = tmp;

  /* Inpaint strokes in labels using a watershed transform */
  labels.watershed (ld, true);

  graph = gegl_node_new ();
  input = gegl_node_new_child (graph,
                               "operation", "gegl:buffer-source",
                               "buffer", _labels,
                               NULL);
  aux = gegl_node_new_child (graph,
                             "operation", "gegl:buffer-source",
                             "buffer", distmap,
                             NULL);
  op  = gegl_node_new_child (graph,
                             "operation", "gegl:watershed-transform",
                             NULL);
  sink  = gegl_node_new_child (graph,
                               "operation", "gegl:buffer-sink",
                               "buffer", &tmp,
                               NULL);
  gegl_node_connect_to (input, "output",
                        op, "input");
  gegl_node_connect_to (aux, "output",
                        op, "aux");
  gegl_node_connect_to (op, "output",
                        sink, "input");
  gegl_node_process (sink);
  g_object_unref (graph);
  g_object_unref (_labels);
  _labels = tmp;

  /* Compare result of CImg and GEGL watersheds.
   * They give different results, but I keep them this way for now. Let's see
   * if GEGL watershed is acceptable on real examples later.
   * TODO: remove!
   */
    {
      gint diff_count = 0;
      gint count = 0;

      for (gint i = 0; i < gegl_buffer_get_width (_labels); i++)
        {
          for (gint j = 0; j < gegl_buffer_get_height (_labels); j++)
            {
              guint32 gegl_label[2];
              ulong   cimg_label = labels (i, j);

              gegl_buffer_sample (_labels, i, j, NULL, gegl_label, NULL,
                                  GEGL_SAMPLER_NEAREST, GEGL_ABYSS_NONE);
              if (gegl_label[0] != (guint32) cimg_label)
                diff_count++;
              count++;
            }
        }
      if (diff_count)
        printf("%d/%d labels different after watershed\n", diff_count, count);
      else
        printf("Labels same after watershed\n");

      /* Fill CImg labels with GEGL watershed results for further comparisons. */
      for (gint i = 0; i < gegl_buffer_get_width (_labels); i++)
        {
          for (gint j = 0; j < gegl_buffer_get_height (_labels); j++)
            {
              guint32 gegl_label2[2];

              gegl_buffer_sample (_labels, i, j, NULL, gegl_label2, NULL,
                                  GEGL_SAMPLER_NEAREST, GEGL_ABYSS_NONE);

              labels (i, j) = gegl_label2[0];
            }
        }
    }

  if (params.removeLargestLabel)
    {
      removeLargestLabel (_labels);
      cimg_removeLargestLabel (labels);

      /* Compare CImg and GEGL labels. */
        {
          gint diff_count = 0;
          gint count = 0;

          for (gint i = 0; i < gegl_buffer_get_width (_labels); i++)
            {
              for (gint j = 0; j < gegl_buffer_get_height (_labels); j++)
                {
                  guint32 gegl_label;
                  ulong   cimg_label = labels (i, j);

                  gegl_buffer_sample (_labels, i, j, NULL, &gegl_label, babl_format ("Y u32"),
                                      GEGL_SAMPLER_NEAREST, GEGL_ABYSS_NONE);

                  if (gegl_label != (guint32) cimg_label)
                    {
                      //printf ("Labels (%d, %d) are different (%d vs %d)!\n",
                      //i, j, gegl_label, cimg_label);

                      diff_count++;
                    }
                  count++;
                }
            }
          if (diff_count)
            printf("%d/%d labels different after removing largest\n", diff_count, count);
          else
            printf("Labels same after removing largest\n");
          gimp_assert (diff_count == 0);
        }
    }
  cimg_library::CImg<uchar> colorMap = cimg_toonColorMap (maxLabel + 1, params.removeLargestLabel, params.maximalColorSaturation, params.minimalColorIntensity);
  gdouble *color_map = toonColorMap (maxLabel + 1, params.removeLargestLabel, params.maximalColorSaturation, params.minimalColorIntensity);
  /* TODO: remove! */
  for (int i = 0; i <= maxLabel; i++)
    {
      colorMap (i, 0, 0, 0) = (uchar) color_map[i * 3];
      colorMap (i, 0, 0, 1) = (uchar) color_map[i * 3 + 1];
      colorMap (i, 0, 0, 2) = (uchar) color_map[i * 3 + 2];
    }
  colorLayer = labels.get_map (colorMap);
  *color_layer = gimp_get_map (_labels, color_map, maxLabel + 1, 3);
  gimp_assert (compare_cimg_gegl_buffers (colorLayer, *color_layer,
                                          "Comparing color layers"));

  g_object_unref (_labels);
  g_object_unref (distmap);
  g_free (color_map);

  return color_buffer;
}

static void
deriche (GeglBuffer *buffer,
         const float sigma,
         gboolean    x_axis,
         const bool  boundary_conditions)
{
  gint width  = gegl_buffer_get_width (buffer);
  gint height = gegl_buffer_get_height (buffer);

  const float nsigma = sigma >= 0 ? sigma : -sigma * (x_axis ? width: height) / 100;
  if (nsigma < 0.1f)
    return;

  const float nnsigma = nsigma < 0.1f ? 0.1f : nsigma;
  const float alpha   = 1.695f / nnsigma;
  const float ema     = (float) expf (-alpha);
  const float ema2    = (float) expf (-2 * alpha);
  const float b1      = -2 * ema;
  const float b2      = ema2;
  float a0 = 0, a1 = 0, a2 = 0, a3 = 0, coefp = 0, coefn = 0;

  /* Order 0 */
  const float k = (1 - ema) * (1 - ema) / (1 + 2 * alpha * ema - ema2);
  a0 = k;
  a1 = k * (alpha - 1) * ema;
  a2 = k * (alpha + 1) * ema;
  a3 = -k * ema2;
  /* Order 0 END */

  coefp = (a0 + a1) / (1 + b1 + b2);
  coefn = (a2 + a3) / (1 + b1 + b2);
  if (x_axis)
    {
      for (int y = 0; y < height; ++y)
        {
          gfloat val;
          gint   x = 0;

          gegl_buffer_sample (buffer, x, y, NULL, &val,
                              babl_format ("Y' float"),
                              GEGL_SAMPLER_NEAREST, GEGL_ABYSS_NONE);

          gfloat Y[width];
          gint y_idx = 0;
          float yb = 0;
          float yp = 0;
          float xp = 0;
          if (boundary_conditions)
            {
              xp = val;
              yb = yp = (float) (coefp * xp);
            }
          for (int m = 0; m < width; ++m)
            {
              gegl_buffer_sample (buffer, x, y, NULL, &val,
                                  babl_format ("Y' float"),
                                  GEGL_SAMPLER_NEAREST, GEGL_ABYSS_NONE);
              const float xc = val;
              x++;
              const float yc = Y[y_idx++] = (float) (a0 * xc + a1 * xp - b1 * yp - b2 * yb);

              xp = xc;
              yb = yp;
              yp = yc;
            }
          float xn = 0;
          float xa = 0;
          float yn = 0;
          float ya = 0;
          if (boundary_conditions)
            {
              gegl_buffer_sample (buffer, x - 1, y, NULL, &val,
                                  babl_format ("Y' float"),
                                  GEGL_SAMPLER_NEAREST, GEGL_ABYSS_NONE);
              xn = xa = val;
              yn = ya = (float) coefn * xn;
            }
          for (int n = width - 1; n >= 0; --n)
            {
              x--;
              gegl_buffer_sample (buffer, x, y, NULL, &val,
                                  babl_format ("Y' float"),
                                  GEGL_SAMPLER_NEAREST, GEGL_ABYSS_NONE);
              const float xc = val;
              const float yc = (float) (a2 * xn + a3 * xa - b1 * yn - b2 * ya);

              xa = xn;
              xn = xc;
              ya = yn;
              yn = yc;
              val = (float) (Y[--y_idx] + yc);
              gegl_buffer_set (buffer, GEGL_RECTANGLE (x, y, 1, 1), 0,
                               babl_format ("Y' float"), &val, GEGL_AUTO_ROWSTRIDE);
            }
        }
    }
  else
    {
      for (int x = 0; x < width; ++x)
        {
          gfloat val;
          gint   y = 0;

          gegl_buffer_sample (buffer, x, y, NULL, &val,
                              babl_format ("Y' float"),
                              GEGL_SAMPLER_NEAREST, GEGL_ABYSS_NONE);

          gfloat Y[height];
          gint y_idx = 0;
          float yb = 0;
          float yp = 0;
          float xp = 0;
          if (boundary_conditions)
            {
              xp = val;
              yb = yp = (float) (coefp * xp);
            }
          for (int m = 0; m < height; ++m)
            {
              gegl_buffer_sample (buffer, x, y, NULL, &val,
                                  babl_format ("Y' float"),
                                  GEGL_SAMPLER_NEAREST, GEGL_ABYSS_NONE);
              const float xc = val;
              y++;
              const float yc = Y[y_idx++] = (float) (a0 * xc + a1 * xp - b1 * yp - b2 * yb);

              xp = xc;
              yb = yp;
              yp = yc;
            }
          float xn = 0;
          float xa = 0;
          float yn = 0;
          float ya = 0;
          if (boundary_conditions)
            {
              gegl_buffer_sample (buffer, x, y - 1, NULL, &val,
                                  babl_format ("Y' float"),
                                  GEGL_SAMPLER_NEAREST, GEGL_ABYSS_NONE);
              xn = xa = val;
              yn = ya = (float)coefn*xn;
            }
          for (int n = height - 1; n >= 0; --n)
            {
              y--;
              gegl_buffer_sample (buffer, x, y, NULL, &val,
                                  babl_format ("Y' float"),
                                  GEGL_SAMPLER_NEAREST, GEGL_ABYSS_NONE);
              const float xc = val;
              const float yc = (float) (a2 * xn + a3 * xa - b1 * yn - b2 * ya);

              xa = xn;
              xn = xc;
              ya = yn;
              yn = yc;
              val = (float) (Y[--y_idx] + yc);
              gegl_buffer_set (buffer, GEGL_RECTANGLE (x, y, 1, 1), 0,
                               babl_format ("Y' float"), &val, GEGL_AUTO_ROWSTRIDE);
            }
        }
    }
}

GeglBuffer *
gimp_get_blur (GeglBuffer* buffer,
               gfloat      sigma)
{
  /* Cf. CImg get_blur() with boundary_conditions = TRUE and is_gaussian =
   * FALSE
   */
  GeglBuffer *res = gegl_buffer_dup (buffer);
                                     //babl_format ("Y' float"));
  gint        width = gegl_buffer_get_width (buffer);
  gint        height = gegl_buffer_get_height (buffer);

  sigma = sigma >= 0.0 ? sigma : -sigma * MAX (width, height) / 100;

  if (width > 1)
    deriche (res, sigma, TRUE, TRUE);
  if (height > 1)
    deriche (res, sigma, FALSE, TRUE);

  return res;
}

typedef
struct PriorityPoint
{
  guint8 priority;

  gint   x;
  gint   y;
} PriorityPoint;

static gint
priority_point_cmp (PriorityPoint *p1,
                    PriorityPoint *p2,
                    gpointer       user_data)
{
  if (p1->priority > p2->priority)
    return -1;
  else if (p2->priority < p1->priority)
    return 1;
  else
    return 0;
}

static gboolean
_priority_queue_insert (GQueue     *Q,
                        GeglBuffer *is_queued,
                        //guint32    &siz,
                        guint8      value,
                        const unsigned int x,
                        const unsigned int y,
                        guint32     n)
{
  guint32 val;

  gegl_buffer_sample (is_queued, x, y, NULL, &val,
                      babl_format ("Y u32"),
                      GEGL_SAMPLER_NEAREST, GEGL_ABYSS_NONE);
  if (val)
    return FALSE;

  gegl_buffer_set (is_queued, GEGL_RECTANGLE (x, y, 1, 1), 0,
                   babl_format ("Y u32"), &n, GEGL_AUTO_ROWSTRIDE);

  PriorityPoint *p = g_new (PriorityPoint, 1);
  p->priority = value;
  p->x = x;
  p->y = y;
  g_queue_insert_sorted (Q, p, (GCompareDataFunc) priority_point_cmp, NULL);

#if 0
  if (++siz >= _width)
    {
      if (!is_empty())
        resize (_width * 2, 3, 1, 1, 0);
      else
        assign (64, 3);
    }
  (*this)(siz - 1, 0) = value;
  (*this)(siz - 1, 1) = (T)x;
  (*this)(siz - 1, 2) = (T)y;
  for (unsigned int pos = siz - 1, par = 0; pos && value > (*this) (par = (pos + 1) / 2 - 1, 0); pos = par)
    {
      cimg::swap((*this)(pos, 0), (*this)(par, 0));
      cimg::swap((*this)(pos, 1), (*this)(par, 1));
      cimg::swap((*this)(pos, 2), (*this)(par, 2));
    }
#endif
  return TRUE;
}

#if 0
static void
_priority_queue_remove (guint8       **Q,
                        unsigned int  &siz)
{
  (*this)(0,0) = (*this)(--siz,0);
  (*this)(0,1) = (*this)(siz,1);
  (*this)(0,2) = (*this)(siz,2);
  (*this)(0,3) = (*this)(siz,3);

  const float value = (*this)(0,0);
  for (unsigned int pos = 0, left = 0, right = 0;
       ((right = 2 * (pos + 1), (left = right - 1)) < siz && value < (*this)(left, 0)) ||
       (right < siz && value < (*this)(right,0));)
    {
      if (right < siz)
        {
          if ((*this)(left,0) > (*this)(right,0))
            {
              cimg::swap((*this)(pos,0),(*this)(left,0));
              cimg::swap((*this)(pos,1),(*this)(left,1));
              cimg::swap((*this)(pos,2),(*this)(left,2));
              cimg::swap((*this)(pos,3),(*this)(left,3));
              pos = left;
            }
          else
            {
              cimg::swap((*this)(pos,0),(*this)(right,0));
              cimg::swap((*this)(pos,1),(*this)(right,1));
              cimg::swap((*this)(pos,2),(*this)(right,2));
              cimg::swap((*this)(pos,3),(*this)(right,3));
              pos = right;
            }
        }
      else
        {
          cimg::swap((*this)(pos,0),(*this)(left,0));
          cimg::swap((*this)(pos,1),(*this)(left,1));
          cimg::swap((*this)(pos,2),(*this)(left,2));
          cimg::swap((*this)(pos,3),(*this)(left,3));
          pos = left;
        }
    }
}
#endif

static void
watershed (GeglBuffer *labels, /* YA u32 */ // const
           GeglBuffer *priority, /* Y u8 */ // const
           gboolean    is_high_connectivity)
{
#define _watershed_init(cond,X,Y) \
  gegl_buffer_sample (labels, X, Y, NULL, label, \
                      babl_format ("YA u32"), \
                      GEGL_SAMPLER_NEAREST, GEGL_ABYSS_NONE); \
  if (cond && ! label[0]) \
    { \
      guint8 prio; \
      gegl_buffer_sample (priority, X, Y, NULL, &prio, \
                          babl_format ("Y u8"), \
                          GEGL_SAMPLER_NEAREST, GEGL_ABYSS_NONE); \
      _priority_queue_insert (Q, _labels, /*sizeQ,*/ prio, X, Y, nb_seeds); \
    }

#define sqr(a) (a*a)
#define _watershed_propagate(cond,X,Y) \
  if (cond) \
    { \
      gegl_buffer_sample (labels, X, Y, NULL, label, \
                          babl_format ("YA u32"), \
                          GEGL_SAMPLER_NEAREST, GEGL_ABYSS_NONE); \
      if (label[0]) \
        { \
          gegl_buffer_sample (_labels, X, Y, NULL, &ns, \
                              babl_format ("Y u32"), \
                              GEGL_SAMPLER_NEAREST, GEGL_ABYSS_NONE); \
          ns--; \
          gegl_buffer_sample (seeds, ns, 0, NULL, &xs, \
                              babl_format ("Y u32"), \
                              GEGL_SAMPLER_NEAREST, GEGL_ABYSS_NONE); \
          gegl_buffer_sample (seeds, ns, 1, NULL, &ys, \
                              babl_format ("Y u32"), \
                              GEGL_SAMPLER_NEAREST, GEGL_ABYSS_NONE); \
          d = sqr((float)x - xs) + sqr((float)y - ys); \
          if (d < dmin) \
            { \
              dmin = d; \
              nmin = ns; \
              gegl_buffer_sample (labels, xs, ys, NULL, &_label, \
                                  babl_format ("Y u32"), \
                                  GEGL_SAMPLER_NEAREST, GEGL_ABYSS_NONE); \
            } \
        } \
      else \
        { \
          guint8 prio; \
          gegl_buffer_sample (priority, X, Y, NULL, &prio, \
                              babl_format ("Y u8"), \
                              GEGL_SAMPLER_NEAREST, GEGL_ABYSS_NONE); \
          _priority_queue_insert(Q, _labels, /*sizeQ, */ prio, X, Y, n); \
        } \
    }

  gimp_assert (gegl_buffer_get_width (labels) == gegl_buffer_get_width (priority) &&
               gegl_buffer_get_height (labels) == gegl_buffer_get_height (priority));

  GeglBuffer *_labels = gegl_buffer_new (gegl_buffer_get_extent (labels), babl_format ("Y u32"));
  GeglBuffer *seeds = gegl_buffer_new (GEGL_RECTANGLE (0, 0, 64, 2), babl_format ("Y u32"));
  gint        width  = gegl_buffer_get_width (labels);
  gint        height = gegl_buffer_get_height (labels);
  guint32     val = 0;
  guint32     label[2];

  gegl_buffer_set_color_from_pixel (_labels, NULL, (const guchar*) &val, babl_format ("Y u32"));

  //CImg<typename cimg::superset2<uint32,uint8,int>::type> Q;
  GQueue   *Q = g_queue_new ();
  unsigned int sizeQ = 0;
  int  px, nx, py, ny;
  bool is_px, is_nx, is_py, is_ny;

  // Find seed points and insert them in priority queue.
  guint32 nb_seeds = 0;
  for (int y = 0; y < height; ++y)
    for (int x = 0; x < width; ++x)
      {
        gegl_buffer_sample (labels, x, y, NULL, label,
                            babl_format ("YA u32"),
                            GEGL_SAMPLER_NEAREST, GEGL_ABYSS_NONE);
        if (label[1])
          { // 3d version
            if (nb_seeds >= gegl_buffer_get_width (seeds))
              {
                GeglBuffer *_seeds = gegl_buffer_new (GEGL_RECTANGLE (0, 0,
                                                                      gegl_buffer_get_width (seeds) * 2, 2),
                                                      babl_format ("Y u32"));
                val = 0;
                gegl_buffer_set_color_from_pixel (_seeds, NULL, (const guchar*) &val, babl_format ("Y u32"));
                gegl_buffer_copy (seeds, NULL, GEGL_ABYSS_NONE, _seeds, NULL);
                g_object_unref (seeds);
                seeds = _seeds;
              }
            val = x;
            gegl_buffer_set (seeds, GEGL_RECTANGLE (nb_seeds, 0, 1, 1), 0,
                             babl_format ("Y u32"), &val, GEGL_AUTO_ROWSTRIDE);
            val = y;
            gegl_buffer_set (seeds, GEGL_RECTANGLE (nb_seeds, 1, 1, 1), 0,
                             babl_format ("Y u32"), &val, GEGL_AUTO_ROWSTRIDE);
            nb_seeds++;
            px = x - 1;
            nx = x + 1;
            py = y - 1;
            ny = y + 1;
            is_px = px >= 0;
            is_nx = nx < width;
            is_py = py >= 0;
            is_ny = ny < height;

            _watershed_init (is_px, px, y);
            _watershed_init (is_nx, nx, y);
            _watershed_init (is_py, x, py);
            _watershed_init (is_ny, x, ny);
            if (is_high_connectivity)
              {
                _watershed_init (is_px && is_py, px, py);
                _watershed_init (is_nx && is_py, nx, py);
                _watershed_init (is_px && is_ny, px, ny);
                _watershed_init (is_nx && is_ny, nx, ny);
              }

            val = nb_seeds;
            gegl_buffer_set (_labels, GEGL_RECTANGLE (x, y, 1, 1), 0,
                             babl_format ("Y u32"), &val, GEGL_AUTO_ROWSTRIDE);
          }
      }

  // Start watershed computation.
  gint updated = 0;
  PriorityPoint *p = NULL;
  //while (sizeQ)
  while ((p = (PriorityPoint *) g_queue_pop_head (Q)))
    {
      // Get and remove point with maximal priority from the queue.
      const int x = p->x;
      const int y = p->y;
      guint32   n;

      g_free (p);
      gegl_buffer_sample (_labels, x, y, NULL, &n,
                          babl_format ("Y u32"),
                          GEGL_SAMPLER_NEAREST, GEGL_ABYSS_NONE);
      px = x - 1; nx = x + 1;
      py = y - 1; ny = y + 1;
      is_px = px>=0; is_nx = nx < width;
      is_py = py>=0; is_ny = ny < height;

      // Check labels of the neighbors.
      //Q._priority_queue_remove(sizeQ);

      guint32 xs, ys, ns, nmin = 0;
      float d, dmin = cimg::type<float>::inf();
      guint32 _label = 0;
      _watershed_propagate (is_px, px, y);
      _watershed_propagate (is_nx, nx, y);
      _watershed_propagate (is_py, x, py);
      _watershed_propagate (is_ny, x, ny);
      if (is_high_connectivity)
        {
          _watershed_propagate (is_px && is_py, px, py);
          _watershed_propagate (is_nx && is_py, nx, py);
          _watershed_propagate (is_px && is_ny, px, ny);
          _watershed_propagate (is_nx && is_ny, nx, ny);
        }
      label[0] = _label;
      label[1] = 1;
      updated++;
      gegl_buffer_set (labels, GEGL_RECTANGLE (x, y, 1, 1), 0,
                       babl_format ("YA u32"), label, GEGL_AUTO_ROWSTRIDE);
      ++nmin;
      gegl_buffer_set (_labels, GEGL_RECTANGLE (x, y, 1, 1), 0,
                       babl_format ("Y u32"), &nmin, GEGL_AUTO_ROWSTRIDE);
    }
  printf("%d pixels updated by watersheding\n");
}

void
colorLayerFromColorSpots (const cimg_library::CImg<uchar> &lineArt,
                          GeglBuffer *line_art,
                          const cimg_library::CImg<uchar> &colorSpots,
                          GeglBuffer *color_spots,
                          cimg_library::CImg<uchar>       &colorLayer)
{
  GeglBuffer  *labellized_spots;
  GeglBuffer  *tmp;
  gboolean     _grayscale;
  const Babl  *spot_format = gegl_buffer_get_format (color_spots);
  const gchar *spot_format_name = babl_get_name (babl_format_get_model (spot_format));
  gint         width  = gegl_buffer_get_width (color_spots);
  gint         height = gegl_buffer_get_height (color_spots);
  guint32      max_spots_label;

  gimp_assert (colorSpots.spectrum () == 2 || colorSpots.spectrum () == 4);
  gimp_assert (colorSpots.width () == lineArt.width () && colorSpots.height () == lineArt.height ());

  gimp_assert (babl_format_has_alpha (spot_format));
  gimp_assert (gegl_buffer_get_width (color_spots) == gegl_buffer_get_width (line_art) &&
               gegl_buffer_get_height (color_spots) == gegl_buffer_get_height (line_art));

  printf ("Color spots format is: %s (%s)\n", babl_get_name (gegl_buffer_get_format (color_spots)),
          spot_format_name);

  //
  // Build a labeling of the spots
  //
  CImg<float> labelizedSpots (colorSpots.width (), colorSpots.height ());
  const bool grayscale = (colorSpots.spectrum () == 2);

  labellized_spots = gegl_buffer_new (gegl_buffer_get_extent (color_spots),
                                      babl_format ("Y u32"));

  /* See gimp_babl_format_get_base_type() in app/gegl/gimp-babl.c */
  _grayscale = (! strcmp (spot_format_name, "Y")   || ! strcmp (spot_format_name, "Y'")  ||
                ! strcmp (spot_format_name, "Y~")  || ! strcmp (spot_format_name, "YA")  ||
                ! strcmp (spot_format_name, "Y'A") || ! strcmp (spot_format_name, "Y~A"));
  gimp_assert (grayscale == _grayscale);

  if (_grayscale)
    {
      for (int y = 0; y < height; ++y)
        for (int x = 0; x < width; ++x)
          {
            guint32 spot[2];
            guint32 label;

            gegl_buffer_sample (color_spots, x, y, NULL, &spot,
                                babl_format ("YA u32"), GEGL_SAMPLER_NEAREST, GEGL_ABYSS_NONE);
            label = (spot[1] < G_MAXUINT32) ? 0 : spot[0];
            gegl_buffer_set (labellized_spots, GEGL_RECTANGLE (x, y, 1, 1), 0,
                             babl_format ("Y u32"), &label, GEGL_AUTO_ROWSTRIDE);

            labelizedSpots (x, y) = (colorSpots (x, y, 0, 1) < 255) ? 0 : colorSpots (x, y);
          }
    }
  else
    {
      for (int y = 0; y < height; ++y)
        for (int x = 0; x < width; ++x)
          {
            guint8  spot[4];
            guint32 label;

            gegl_buffer_sample (color_spots, x, y, NULL, &spot,
                                babl_format ("R'G'B'A u8"), GEGL_SAMPLER_NEAREST, GEGL_ABYSS_NONE);
            label = (spot[3] < G_MAXUINT8) ? 0 : (guint32) strangeNorm (spot[0], spot[1], spot[2]);
            gegl_buffer_set (labellized_spots, GEGL_RECTANGLE (x, y, 1, 1), 0,
                             babl_format ("Y u32"), &label, GEGL_AUTO_ROWSTRIDE);

            labelizedSpots (x, y) = (colorSpots (x, y, 0, 3) < 255) ? 0 : strangeNorm (colorSpots (x, y, 0, 0),
                                                                                       colorSpots (x, y, 0, 1),
                                                                                       colorSpots (x, y, 0, 2));
          }
    }
    {
      gint diff_count = 0;
      gint count = 0;
      for (int y = 0; y < height; ++y)
        for (int x = 0; x < width; ++x)
          {
            guint32 label;

            gegl_buffer_sample (labellized_spots, x, y, NULL, &label,
                                babl_format ("Y u32"), GEGL_SAMPLER_NEAREST, GEGL_ABYSS_NONE);
            if (label != (guint32) labelizedSpots (x, y))
              {
                printf ("Different label spots at (%d, %d)): %d vs %f\n",
                        x, y, label, labelizedSpots (x, y));
                diff_count++;
              }
            count++;
          }
      if (diff_count)
        printf("%d/%d label different at creation\n", diff_count, count);
      gimp_assert (diff_count == 0);
    }

  tmp = gimp_get_labels (labellized_spots, TRUE);
  g_object_unref (labellized_spots);
  labellized_spots = tmp;

  labelizedSpots.label (true);
    {
      gint diff_count = 0;
      gint count = 0;
      for (int y = 0; y < height; ++y)
        for (int x = 0; x < width; ++x)
          {
            guint32 label;

            gegl_buffer_sample (labellized_spots, x, y, NULL, &label,
                                babl_format ("Y u32"), GEGL_SAMPLER_NEAREST, GEGL_ABYSS_NONE);
            if (label != (guint32) labelizedSpots (x, y))
              diff_count++;
            count++;
          }
      if (diff_count)
        printf("%d/%d label different after get_label()\n", diff_count, count);
      gimp_assert (diff_count == 0);
    }

  const long maxSpotsLabel = labelizedSpots.max ();
    {
      GeglBufferIterator *gi;

      max_spots_label = 0;
      gi = gegl_buffer_iterator_new (labellized_spots, NULL, 0, NULL,
                                     GEGL_ACCESS_READ, GEGL_ABYSS_NONE, 1);
      while (gegl_buffer_iterator_next (gi))
        {
          guint32 *data = (guint32*) gi->items[0].data;
          gint    k;

          for (k = 0; k < gi->length; k++)
            {
              max_spots_label = MAX (*data, max_spots_label);
              data++;
            }
        }
    }
  gimp_assert (max_spots_label == (guint32) maxSpotsLabel);

  //
  // Build the colormap (average colors of the spots)
  //
  gfloat *_colors = g_new (gfloat, (max_spots_label + 1) * (_grayscale ? 1 : 3));
  glong  *_h = g_new (glong, max_spots_label + 1);

  memset (_colors, 0, sizeof (gfloat) * (max_spots_label + 1) * (_grayscale ? 1 : 3));
  memset (_h, 0, sizeof (glong) * (max_spots_label + 1));

  CImg<float> colors (maxSpotsLabel + 1, 1, 1, colorSpots.spectrum () - 1);
  CImg<long> h (maxSpotsLabel + 1);
  memset (colors.data (), 0, sizeof (float) * colors.size ());
  memset (h.data (), 0, sizeof (long) * h.size ());
  if (grayscale)
    {
      for (int y = 0; y < height; ++y)
        for (int x = 0; x < width; ++x)
          {
            const int label = labelizedSpots (x, y);
            guint32 _label;

            gegl_buffer_sample (labellized_spots, x, y, NULL, &_label,
                                babl_format ("Y u32"),
                                GEGL_SAMPLER_NEAREST, GEGL_ABYSS_NONE);
            gimp_assert ((guint32) label == _label);
            if (label)
              {
                guint32 spot;

                gegl_buffer_sample (color_spots, x, y, NULL, &spot,
                                    babl_format ("Y u32"),
                                    GEGL_SAMPLER_NEAREST, GEGL_ABYSS_NONE);
                _colors[(gint) label] += spot;

                colors (label, 0, 0, 0) += colorSpots (x, y, 0, 0);

                _h[(gint) label] += 1;
                h (label) += 1;
              }
          }
      _h[0] = 1;
      h (0) = 1;
      for (int  x = 0;  x < maxSpotsLabel + 1; ++x)
        {
          _colors[x] /= _h[x];

          colors (x) /= h (x);
        }
    }
  else
    {
      for (int y = 0; y < height; ++y)
        for (int x = 0; x < width; ++x)
          {
            const int label = labelizedSpots (x, y);
            guint32 _label;

            gegl_buffer_sample (labellized_spots, x, y, NULL, &_label,
                                babl_format ("Y u32"),
                                GEGL_SAMPLER_NEAREST, GEGL_ABYSS_NONE);
            gimp_assert ((guint32) label == _label);
            if (_label)
              {
                guint8 spot[3];

                gegl_buffer_sample (color_spots, x, y, NULL, &spot,
                                    babl_format ("R'G'B' u8"),
                                    GEGL_SAMPLER_NEAREST, GEGL_ABYSS_NONE);
                _colors[(gint) label * 3] += spot[0];
                _colors[(gint) label * 3 + 1] += spot[1];
                _colors[(gint) label * 3 + 2] += spot[2];
                _h[(gint) label] += 1;

                colors (label, 0, 0, 0) += colorSpots (x, y, 0, 0);
                colors (label, 0, 0, 1) += colorSpots (x, y, 0, 1);
                colors (label, 0, 0, 2) += colorSpots (x, y, 0, 2);
                h (label) += 1;
              }
          }
      _h[0] = 1;
      h (0) = 1;
      for (int x = 0; x < maxSpotsLabel + 1; ++ x)
        {
          _colors[x * 3]     /= _h[x];
          _colors[x * 3 + 1] /= _h[x];
          _colors[x * 3 + 2] /= _h[x];

          colors (x, 0, 0, 0) /= h (x);
          colors (x, 0, 0, 1) /= h (x);
          colors (x, 0, 0, 2) /= h (x);
        }
    }
  for (int x = 0; x < maxSpotsLabel + 1; ++ x)
    {
      gimp_assert (_colors[x * 3] == colors (x, 0, 0, 0)         &&
                   _colors[x * 3 + 1] == colors (x, 0, 0, 1) &&
                   _colors[x * 3 + 2] == colors (x, 0, 0, 2) &&
                   _h[x] == h(x));
    }

  // Watershed labels with special priority
  CImg<float> power = (lineArt * -1).normalize (0, 1);
  GeglBuffer *_power = gegl_buffer_new (gegl_buffer_get_extent (line_art),
                                        babl_format ("Y' float"));
    {
      GeglBufferIterator *gi;
      guint8              min = 0;
      guint8              max = 0;

      gi = gegl_buffer_iterator_new (line_art, NULL, 0, babl_format ("Y' u8"),
                                     GEGL_ACCESS_READ, GEGL_ABYSS_NONE, 1);
      while (gegl_buffer_iterator_next (gi))
        {
          guint8 *data = (guint8*) gi->items[0].data;
          gint    k;

          for (k = 0; k < gi->length; k++)
            {
              max = MAX (*data, max);
              min = MIN (*data, min);
              data++;
            }
        }

      gi = gegl_buffer_iterator_new (line_art, NULL, 0, babl_format ("Y' u8"),
                                     GEGL_ACCESS_READ, GEGL_ABYSS_NONE, 2);
      gegl_buffer_iterator_add (gi, _power, NULL, 0, babl_format ("Y' float"),
                                GEGL_ACCESS_WRITE, GEGL_ABYSS_NONE);
      while (gegl_buffer_iterator_next (gi))
        {
          guint8 *in = (guint8*) gi->items[0].data;
          gfloat *out = (gfloat*) gi->items[1].data;
          gint    k;

          for (k = 0; k < gi->length; k++)
            {
              *out = ((gfloat) -*in + max)/((gfloat) max - min);
              out++;
              in++;
            }
        }
      /* Test of similarity. */
      gint     diff_count = 0;
      gint     count = 0;
      for (int y = 0; y < height; ++y)
        for (int x = 0; x < width; ++x)
          {
            gfloat label;

            gegl_buffer_sample (_power, x, y, NULL, &label,
                                babl_format ("Y' float"),
                                GEGL_SAMPLER_NEAREST, GEGL_ABYSS_NONE);
            if (ABS (label - power (x, y)) > 0.000001)
              {
                printf("Label (%d %d): %f vs %f\n", x, y, label, power (x,y));
                diff_count++;
              }
            count++;
          }
      if (diff_count)
        printf("%d/%d label different after normalization\n", diff_count, count);
      gimp_assert (diff_count == 0);
    }
  power = convert_gegl_to_cimg<float> (_power);
    {
      GeglNode *graph;
      GeglNode *input;
      GeglNode *sink;

      graph = gegl_node_new ();
      input = gegl_node_new_child (graph,
                                   "operation", "gegl:buffer-source",
                                   "buffer", _power,
                                   NULL);
      sink  = gegl_node_new_child (graph,
                                   "operation", "gegl:save",
                                   "path", "power.png",
                                   NULL);
      gegl_node_connect_to (input, "output",
                            sink, "input");
      gegl_node_process (sink);
      g_object_unref (graph);
    }

  CImg<float> b1p = power.get_blur (0.01 * MAX (power.width (), power.height ()));
  GeglBuffer *_b1p;
  _b1p = gimp_get_blur (_power, 0.01 * MAX (width, height));
    {
      /* Test of similarity. */
      gint     diff_count = 0;
      gint     count = 0;
      for (int y = 0; y < height; ++y)
        for (int x = 0; x < width; ++x)
          {
            gfloat label;

            gegl_buffer_sample (_b1p, x, y, NULL, &label,
                                babl_format ("Y' float"),
                                GEGL_SAMPLER_NEAREST, GEGL_ABYSS_NONE);
            if (ABS (label - b1p (x, y)) > 0.00001)
              {
                printf("Label (%d %d): %f vs %f\n", x, y, label, b1p (x,y));
                diff_count++;
              }
            count++;
          }
      if (diff_count)
        printf("%d/%d label different after first blur\n", diff_count, count);
      gimp_assert (diff_count == 0);
    }
    {
      GeglNode *graph;
      GeglNode *input;
      GeglNode *sink;

      graph = gegl_node_new ();
      input = gegl_node_new_child (graph,
                                   "operation", "gegl:buffer-source",
                                   "buffer", _b1p,
                                   NULL);
      sink  = gegl_node_new_child (graph,
                                   "operation", "gegl:save",
                                   "path", "b1p.png",
                                   NULL);
      gegl_node_connect_to (input, "output",
                            sink, "input");
      gegl_node_process (sink);
      g_object_unref (graph);
    }
  b1p = convert_gegl_to_cimg<float> (_b1p);

  GeglBuffer *_blured = gimp_get_blur (_power, 1);
    {
      GeglBufferIterator *gi;
      gi = gegl_buffer_iterator_new (_b1p, NULL, 0, babl_format ("Y' float"),
                                     GEGL_ACCESS_READ, GEGL_ABYSS_NONE, 2);
      gegl_buffer_iterator_add (gi, _blured, NULL, 0, babl_format ("Y' float"),
                                GEGL_ACCESS_WRITE, GEGL_ABYSS_NONE);
      while (gegl_buffer_iterator_next (gi))
        {
          gfloat *in = (gfloat*) gi->items[0].data;
          gfloat *out = (gfloat*) gi->items[1].data;
          gint    k;

          for (k = 0; k < gi->length; k++)
            {
              *out = MIN (*in, *out);
              out++;
              in++;
            }
        }
    }

  CImg<float> blured = power.get_blur (1).min (b1p);
    {
      GeglNode *graph;
      GeglNode *input;
      GeglNode *sink;

      graph = gegl_node_new ();
      input = gegl_node_new_child (graph,
                                   "operation", "gegl:buffer-source",
                                   "buffer", _blured,
                                   NULL);
      sink  = gegl_node_new_child (graph,
                                   "operation", "gegl:save",
                                   "path", "blured.png",
                                   NULL);
      gegl_node_connect_to (input, "output",
                            sink, "input");
      gegl_node_process (sink);
      g_object_unref (graph);
    }
    {
      /* Test of similarity. */
      gint     diff_count = 0;
      gint     count = 0;
      for (int y = 0; y < height; ++y)
        for (int x = 0; x < width; ++x)
          {
            gfloat label;

            gegl_buffer_sample (_blured, x, y, NULL, &label,
                                babl_format ("Y' float"),
                                GEGL_SAMPLER_NEAREST, GEGL_ABYSS_NONE);
            if (ABS (label - blured (x, y)) > 0.000001)
              {
                printf("Label (%d %d): %f vs %f\n", x, y, label, blured (x,y));
                diff_count++;
              }
            count++;
          }
      if (diff_count)
        printf("%d/%d label different after second blur\n", diff_count, count);
      gimp_assert (diff_count == 0);
    }
  blured = convert_gegl_to_cimg<float> (_blured);
  g_object_unref (_b1p);
  g_object_unref (_power);

    {
      gint diff_count = 0;
      gint count = 0;
      gint color_count = 0;
      gint black_count = 0;
      for (int y = 0; y < height; ++y)
        for (int x = 0; x < width; ++x)
          {
            guint32 label;

            gegl_buffer_sample (labellized_spots, x, y, NULL, &label,
                                babl_format ("Y u32"), GEGL_SAMPLER_NEAREST, GEGL_ABYSS_NONE);
            if (label != (guint32) labelizedSpots (x, y))
              diff_count++;
            if (label)
              color_count++;
            else
              black_count++;
            count++;
          }
      if (diff_count)
        printf("%d/%d label different before watershed\n", diff_count, count);
      printf("label same before watershed (%d color %d black)\n", color_count, black_count);
      gimp_assert (diff_count == 0);
    }

    {
      GeglBufferIterator *gi;
      /* Expected input label map needs an alpha channel, whose purpose is only to
       * flag unlabelled pixels (alpha == 0).
       */
      tmp = gegl_buffer_new (gegl_buffer_get_extent (labellized_spots),
                             babl_format ("YA u32"));
      gi = gegl_buffer_iterator_new (labellized_spots, NULL, 0, babl_format ("Y u32"),
                                     GEGL_ACCESS_READ, GEGL_ABYSS_NONE, 2);
      gegl_buffer_iterator_add (gi, tmp, NULL, 0, NULL,
                                GEGL_ACCESS_WRITE, GEGL_ABYSS_NONE);
      gint count = 0;
      while (gegl_buffer_iterator_next (gi))
        {
          guint32 *data = (guint32*) gi->items[0].data;
          guint32 *out  = (guint32*) gi->items[1].data;
          gint     k;

          for (k = 0; k < gi->length; k++)
            {
              out[0] = *data;
              if (*data == 0)
                {
                out[1] = 0;
                count++;
                }
              else
                out[1] = 1;
              data++;
              out += 2;
            }
        }
      printf("%d labels were labelled for watershedding!\n", count);
      g_object_unref (labellized_spots);
      labellized_spots = tmp;
    }

    {
      /* Compare labellized spots before watershed! */
      gint diff_count = 0;
      gint count = 0;

      for (gint i = 0; i < gegl_buffer_get_width (labellized_spots); i++)
        {
          for (gint j = 0; j < gegl_buffer_get_height (labellized_spots); j++)
            {
              guint32 gegl_label[2];
              gfloat cimg_label = labelizedSpots (i, j);

              gegl_buffer_sample (labellized_spots, i, j, NULL, gegl_label, NULL,
                                  GEGL_SAMPLER_NEAREST, GEGL_ABYSS_NONE);
              if (gegl_label[0] != (guint32) cimg_label)
                {
                  diff_count++;
                }

              count++;
            }
        }
      if (diff_count)
        printf("%d/%d labels different before watershed\n", diff_count, count);
      else
        printf("Labels same before watershed\n");
    }

  labelizedSpots.watershed (blured, true);
  //printf("Blured width, height, depth and spectrum: %d %d %d %d\n",
  //blured._width, blured._height, blured._depth, blured._spectrum);
  //gimp_assert(FALSE);
  //watershed (labellized_spots, _blured, TRUE);
#if 0
    {
      GeglNode           *graph;
      GeglNode           *input;
      GeglNode           *aux;
      GeglNode           *op;
      GeglNode           *sink;

      graph = gegl_node_new ();
      input = gegl_node_new_child (graph,
                                   "operation", "gegl:buffer-source",
                                   "buffer", labellized_spots,
                                   NULL);
      aux = gegl_node_new_child (graph,
                                 "operation", "gegl:buffer-source",
                                 "buffer", _blured,
                                 NULL);
      op  = gegl_node_new_child (graph,
                                 "operation", "gegl:watershed-transform",
                                 NULL);
      sink  = gegl_node_new_child (graph,
                                   "operation", "gegl:buffer-sink",
                                   "buffer", &tmp,
                                   NULL);
      gegl_node_connect_to (input, "output",
                            op, "input");
      gegl_node_connect_to (aux, "output",
                            op, "aux");
      gegl_node_connect_to (op, "output",
                            sink, "input");
      gegl_node_process (sink);
      g_object_unref (graph);
      g_object_unref (labellized_spots);
      labellized_spots = tmp;
    }
#endif
#if 0
    {
      /* Compare labellized spots after watershed! */
      gint diff_count = 0;
      gint count = 0;

      for (gint i = 0; i < gegl_buffer_get_width (labellized_spots); i++)
        {
          for (gint j = 0; j < gegl_buffer_get_height (labellized_spots); j++)
            {
              guint32 gegl_label[2];
              gfloat cimg_label = labelizedSpots (i, j);

              gegl_buffer_sample (labellized_spots, i, j, NULL, gegl_label, NULL,
                                  GEGL_SAMPLER_NEAREST, GEGL_ABYSS_NONE);
              if (gegl_label[0] != (guint32) cimg_label)
                {
                  diff_count++;
                  labelizedSpots (i, j) = (float) gegl_label[0];
                }

              count++;
            }
        }
      if (diff_count)
        printf("%d/%d labels different after watershed\n", diff_count, count);
      else
        printf("Labels same after watershed\n");
    }
#endif

  colorLayer = labelizedSpots.get_map (colors);

  g_object_unref (labellized_spots);
}

static void
computeNormalsAndCurvature (const CImg<uchar> *mask,
                            GeglBuffer        *buffer,
                            CImg<float>       *normals,
                            CImg<float>       *curvatures,
                            gfloat         ***_normals,
                            gfloat          **_curvatures,
                            int                normalEstimateMaskSize)
{
  GArray  *cimg_es = cimg_gimp_edgelset_new (mask);
  GArray  *es = gimp_edgelset_new (buffer);
  Edgel  **e  = (Edgel **) es->data;

  gimp_assert (gegl_buffer_get_width (buffer) == mask->_width &&
               gegl_buffer_get_height (buffer) == mask->_height);
  gimp_assert (gimp_edgelset_are_same (cimg_es, es));
  g_array_free (cimg_es, TRUE);

  gimp_edgelset_smooth_normals (es, normalEstimateMaskSize);
  gimp_edgelset_compute_curvature (es);
  *curvatures = 0.0f;
  *normals = 0.0f;

  while (*e)
    {
      const float w = MAX (1e-8f, (*e)->curvature * (*e)->curvature);

      _normals[(*e)->x][(*e)->y][0] += w * (*e)->x_normal;
      _normals[(*e)->x][(*e)->y][1] += w * (*e)->y_normal;
      _curvatures[(*e)->x][(*e)->y] = MAX ((*e)->curvature,
                                           _curvatures[(*e)->x][(*e)->y]);

      (*normals) ((*e)->x, (*e)->y, 0) += w * (*e)->x_normal;
      (*normals) ((*e)->x, (*e)->y, 1) += w * (*e)->y_normal;
      (*curvatures) ((*e)->x, (*e)->y) = MAX ((*e)->curvature,
                                              (*curvatures) ((*e)->x, (*e)->y));

      e++;
    }
  for (int y = 0; y < gegl_buffer_get_height (buffer); ++y)
    for (int x = 0; x < gegl_buffer_get_width (buffer); ++x)
      {
        const float _angle = atan2f (_normals[x][y][1], _normals[x][y][0]);
        _normals[x][y][0] = cosf (_angle);
        _normals[x][y][1] = sinf (_angle);

        const float angle = atan2f ((*normals) (x, y, 1), (*normals) (x, y, 0));
        (*normals) (x, y, 0) = cosf (angle);
        (*normals) (x, y, 1) = sinf (angle);
      }
  g_array_free (es, TRUE);
}

static GList *
cimg_findSplineCandidates (GArray                          *maxPositions,
                           const cimg_library::CImg<float> *normals,
                           int                              distanceThreshold,
                           float                            maxAngleDeg)
{
  GList       *candidates = NULL;
  const float  CosMin     = cosf (M_PI * (maxAngleDeg / 180.0));
  gint         i;

  for (i = 0; i < maxPositions->len; i++)
    {
      Pixel p1 = g_array_index (maxPositions, Pixel, i);
      gint  j;

      for (j = i + 1; j < maxPositions->len; j++)
        {
          Pixel       p2 = g_array_index (maxPositions, Pixel, j);
          const float distance = gimp_vector2_length_val (gimp_vector2_sub_val (p1, p2));

          if (distance <= distanceThreshold)
            { // Distance threshold
              GimpVector2 normalP1;
              GimpVector2 normalP2;
              GimpVector2 p1f;
              GimpVector2 p2f;
              GimpVector2 p1p2;
              float       cosN;
              float       qualityA;
              float       qualityB;
              float       qualityC;
              float       quality;

              normalP1 = gimp_vector2_new ((*normals) (p1.x, p1.y, 0), (*normals) (p1.x, p1.y, 1));
              normalP2 = gimp_vector2_new ((*normals) (p2.x, p2.y, 0), (*normals) (p2.x, p2.y, 1));
              p1f = gimp_vector2_new (p1.x, p1.y);
              p2f = gimp_vector2_new (p2.x, p2.y);
              p1p2 = gimp_vector2_sub_val (p2f, p1f);

              cosN = gimp_vector2_inner_product_val (normalP1, (gimp_vector2_neg_val (normalP2)));
              qualityA = MAX (0.0f, 1 - distance / distanceThreshold);
              qualityB = MAX (0.0f,
                              (float) (gimp_vector2_inner_product_val (normalP1, p1p2) - gimp_vector2_inner_product_val (normalP2, p1p2)) /
                              distance);
              qualityC = MAX (0.0f, cosN - CosMin);
              quality = qualityA * qualityB * qualityC;
              if (quality > 0)
                {
                  SplineCandidate *candidate = g_new (SplineCandidate, 1);

                  candidate->p1      = p1;
                  candidate->p2      = p2;
                  candidate->quality = quality;

                  candidates = g_list_insert_sorted_with_data (candidates, candidate,
                                                               (GCompareDataFunc) gimp_spline_candidate_cmp,
                                                               NULL);
                }
            }
        }
    }
  return candidates;
}

GArray *
cimg_curvatureExtremums (CImg<float> & curvatures)
{
  GArray                   *maxPositions;
  cimg_library::CImg<bool>  visited;
  GQueue                   *q = g_queue_new ();

  maxPositions = g_array_new (FALSE, TRUE, sizeof (Pixel));

  visited.assign (curvatures.width (), curvatures.height ());
  memset (visited.data (), 0, visited.size () * sizeof (bool));

  for (int y = 0; y<(int)(visited._height); ++y)
    for (int x = 0; x<(int)(visited._width); ++x)
      {
        if ((curvatures (x, y) > 0.0) && ! visited (x, y))
          {
            size_t  count = 0;
            float   maxCurvature = 0.0;
            Pixel   maxCurvaturePixel = gimp_vector2_new (-1.0, -1.0);
            Pixel  *p = g_new (Pixel, 1);

            p->x = x;
            p->y = y;
            g_queue_push_tail (q, p);
            visited (x, y) = true;

            while (! g_queue_is_empty (q))
              {
                float c;

                p = (Pixel *) g_queue_pop_head (q);
                c = curvatures (p->x, p->y);

                ++count;
                curvatures (p->x, p->y) = 0.0f;
                if (curvatures.containsXYZC (p->x + 1, p->y) &&
                    (curvatures (p->x + 1, p->y) > 0.0) &&
                    ! visited (p->x + 1, p->y))
                  {
                    Pixel *p2 = g_new (Pixel, 1);

                    p2->x = p->x + 1;
                    p2->y = p->y;
                    g_queue_push_tail (q, p2);
                    visited (p->x + 1, p->y) = true;
                  }
                if (curvatures.containsXYZC (p->x - 1, p->y) &&
                    (curvatures (p->x - 1, p->y) > 0.0) &&
                    ! visited (p->x - 1, p->y))
                  {
                    Pixel *p2 = g_new (Pixel, 1);

                    p2->x = p->x - 1;
                    p2->y = p->y;
                    g_queue_push_tail (q, p2);
                    visited (p->x - 1, p->y) = true;
                  }
                if (curvatures.containsXYZC (p->x, p->y - 1) &&
                    (curvatures (p->x, p->y - 1) > 0.0) &&
                    ! visited (p->x, p->y - 1))
                  {
                    Pixel *p2 = g_new (Pixel, 1);

                    p2->x = p->x;
                    p2->y = p->y - 1;
                    g_queue_push_tail (q, p2);
                    visited (p->x, p->y - 1) = true;
                  }
                if (curvatures.containsXYZC (p->x, p->y + 1) &&
                    (curvatures (p->x, p->y + 1) > 0.0) &&
                    ! visited (p->x, p->y + 1))
                  {
                    Pixel *p2 = g_new (Pixel, 1);

                    p2->x = p->x;
                    p2->y = p->y + 1;
                    g_queue_push_tail (q, p2);
                    visited (p->x, p->y + 1) = true;
                  }
                if (curvatures.containsXYZC (p->x + 1, p->y + 1) &&
                    (curvatures (p->x + 1, p->y + 1) > 0.0) &&
                    ! visited (p->x + 1, p->y + 1))
                  {
                    Pixel *p2 = g_new (Pixel, 1);

                    p2->x = p->x + 1;
                    p2->y = p->y + 1;
                    g_queue_push_tail (q, p2);
                    visited (p->x + 1, p->y + 1) = true;
                  }
                if (curvatures.containsXYZC (p->x - 1, p->y - 1) &&
                    (curvatures (p->x - 1, p->y - 1) > 0.0) &&
                    ! visited (p->x - 1, p->y - 1))
                  {
                    Pixel *p2 = g_new (Pixel, 1);

                    p2->x = p->x - 1;
                    p2->y = p->y - 1;
                    g_queue_push_tail (q, p2);
                    visited (p->x - 1, p->y - 1) = true;
                  }
                if (curvatures.containsXYZC (p->x - 1, p->y + 1) &&
                    (curvatures (p->x - 1, p->y + 1) > 0.0) &&
                    ! visited (p->x - 1, p->y + 1))
                  {
                    Pixel *p2 = g_new (Pixel, 1);

                    p2->x = p->x - 1;
                    p2->y = p->y + 1;
                    g_queue_push_tail (q, p2);
                    visited (p->x - 1, p->y + 1) = true;
                  }
                if (curvatures.containsXYZC (p->x + 1, p->y - 1) &&
                    (curvatures (p->x + 1, p->y - 1) > 0.0) &&
                    ! visited (p->x + 1, p->y - 1))
                  {
                    Pixel *p2 = g_new (Pixel, 1);

                    p2->x = p->x + 1;
                    p2->y = p->y - 1;
                    g_queue_push_tail (q, p2);
                    visited (p->x + 1, p->y - 1) = true;
                  }

                if (c > maxCurvature)
                  {
                    maxCurvaturePixel = *p;
                    maxCurvature = c;
                  }
                g_free (p);
              }
            curvatures (maxCurvaturePixel.x, maxCurvaturePixel.y) = maxCurvature;
            g_array_append_val (maxPositions, maxCurvaturePixel);
          }
      }
  g_queue_free_full (q, g_free);

  return maxPositions;
}

float
cimg_medianDistanceMaximumPerConnectedComponent (const cimg_library::CImg<uchar> *mask)
{
  CImg<float> distance = mask->get_distance (0);
  CImg<ulong> labels = mask->get_label ();
  for (int y = 0; y<(int)(mask->_height); ++y)
    for (int x = 0; x<(int)(mask->_width); ++x)
      {
        if (! (*mask) (x, y))
          {
            labels (x, y) = 0;
          }
      }
  CImg<float> dmax (labels.max ());
  dmax.fill (0.0);
  for (int y = 0; y<(int)(distance._height); ++y)
    for (int x = 0; x<(int)(distance._width); ++x)
      {
        if ( (*mask) (x, y))
          {
            gimp_assert (labels (x, y));
            float v = dmax (labels (x, y) - 1);
            float d = distance (x, y);
            if (d > v)
              {
                dmax (labels (x, y) - 1) = d;
              }
          }
      }
  dmax.sort ();
  dmax.autocrop (0.0, "x");
  return dmax (dmax.width () / 2);
}

namespace
{
  inline bool cimg_borderInDirection (const cimg_library::CImg<uchar> & mask, Pixel p, int direction)
    {
      return ! mask.containsXYZC (p.x + DeltaX[direction], p.y + DeltaY[direction]) ||
             ! mask (p.x + DeltaX[direction], p.y + DeltaY[direction]);
    }
} // namespace

static bool
cimg_curveCreatesOneSmallAndSignificantRegion (cimg_library::CImg<uchar> *mask,
                                               GArray                    *pixels,
                                               int                        lowerSizeLimit,
                                               int                        upperSizeLimit)
{
  const long maxEdgelCount = 2 * (upperSizeLimit + 1);
  Pixel     *p = (Pixel*) pixels->data;
  gint       i;

  // Mark pixels
  for (i = 0; i < pixels->len; i++)
    {
      if (mask->containsXYZC (p->x, p->y))
        {
          (*mask) (p->x, p->y) = (*mask) (p->x, p->y) ? 3 : 2;
        }
      p++;
    }

  long count = 0;
  long area;
  for (i = 0; i < pixels->len; i++)
    {
      Pixel p = g_array_index (pixels, Pixel, i);

      for (int direction = 0; direction < 4; ++direction)
        {
          if (mask->containsXYZC (p.x, p.y)           &&
              cimg_borderInDirection (*mask, p, direction) &&
              ! ((*mask) (p.x, p.y) & (4 << direction)))
            {
              Edgel e;

              gimp_edgel_init (&e);
              e.x = p.x;
              e.y = p.y;
              e.direction = static_cast<Direction>(direction);

              count = cimg_trackAndMark (mask, e, maxEdgelCount);
              if ((count != -1) && (count <= maxEdgelCount)              &&
                  ((area = -1 * cimg_regionArea (mask, e)) >= lowerSizeLimit) &&
                  (area <= upperSizeLimit))
                {
                  gint j;

                  // Remove marks
                  for (j = 0; j < pixels->len; j++)
                    {
                      Pixel p2 = g_array_index (pixels, Pixel, j);

                      if (mask->containsXYZC (p2.x, p2.y))
                        {
                          (*mask) (p2.x, p2.y) &= 1;
                        }
                    }
                  return true;
                }
            }
        }
    }

  // Remove marks
  for (i = 0; i < pixels->len; i++)
    {
      Pixel p = g_array_index (pixels, Pixel, i);

      if (mask->containsXYZC (p.x, p.y))
        {
          (*mask) (p.x, p.y) &= 1;
        }
    }
  return false;
}

/*
 *
 *
 *
 */

static bool
significantAlphaChannel (GeglBuffer *buffer)
{
  float min = G_MAXFLOAT;
  float max = G_MINFLOAT;

  if ( ! babl_format_has_alpha (gegl_buffer_get_format (buffer)))
    return FALSE;

  for (gint y = 0; y < gegl_buffer_get_height (buffer); y++)
    for (gint x = 0; x < gegl_buffer_get_width (buffer); x++)
      {
        gfloat pixel;

        gegl_buffer_sample (buffer, x, y, NULL, &pixel, babl_format ("A float"),
                            GEGL_SAMPLER_NEAREST, GEGL_ABYSS_NONE);
        min = MIN (pixel, min);
        max = MAX (pixel, max);
      }

  return (max - min) > 64.0;
}

bool
cimg_significantAlphaChannel (const cimg_library::CImg<float> &image,
                              GeglBuffer                      *buffer)
{
  float min = G_MAXFLOAT;
  float max = G_MINFLOAT;

  float _min = G_MAXFLOAT;
  float _max = G_MINFLOAT;

  gimp_assert (buffer == NULL ||
               image.spectrum () == babl_format_get_n_components (gegl_buffer_get_format (buffer)));
  if (image.spectrum () != 2 &&
      image.spectrum () != 4)
    {
      gimp_assert (buffer == NULL || ! babl_format_has_alpha (gegl_buffer_get_format (buffer)));
      return false;
    }
  gimp_assert (buffer != NULL && babl_format_has_alpha (gegl_buffer_get_format (buffer)));
  /*if (! babl_format_has_alpha (gegl_buffer_get_format (buffer)))
   *  return FALSE;
   */

  cimg_library::CImg<float> alpha = image.get_shared_channel (image.spectrum () - 1);
  for (float *p = (alpha)._data, *_maxp = (alpha)._data + (alpha).size(); p<_maxp; ++p)
    {
      min = MIN (*p, min);
      max = MAX (*p, max);
    }
  for (gint y = 0; y < gegl_buffer_get_height (buffer); y++)
    for (gint x = 0; x < gegl_buffer_get_width (buffer); x++)
      {
        gfloat pixel;

        gegl_buffer_sample (buffer, x, y, NULL, &pixel, babl_format ("A float"),
                            GEGL_SAMPLER_NEAREST, GEGL_ABYSS_NONE);
        _min = MIN (pixel, _min);
        _max = MAX (pixel, _max);
      }
  gimp_assert (min == _min && max == _max);

  return (max - min) > 64.0;
}

/* TODO: remove with GEGL implementation. */
void
rgb2luminance (cimg_library::CImg<float> & img)
{
  cimg_library::CImg<float> result (img.width (), img.height (), 1, 1);
  const size_t channelSize = img.width () * img.height ();
  float * r = img.data ();
  float * g = r + channelSize;
  float * b = g + channelSize;

  gimp_assert (img.spectrum () >= 3);

  for (float *p = (result)._data, *_maxp = (result)._data + (result).size(); p<_maxp; ++p)
    {
      *p = 0.22248840 * (*r) + 0.71690369 * (*g) + 0.06060791 * (*b);
      ++r;
      ++g;
      ++b;
    }
  img.swap (result);
  return;
}

void
cimg_removeSmallLabeledRegions (cimg_library::CImg<ulong> &image,
                                unsigned int               size)
{
  cimg_library::CImg<bool>  visited;
  GQueue                   *q = g_queue_new ();
  GArray                   *region;
  ulong                     label;

  visited.assign (image.width (), image.height ());
  memset (visited.data (), 0, visited.size () * sizeof (bool));

  region = g_array_sized_new (FALSE, TRUE, sizeof (Pixel), size);

  for (int y = 0; y<(int)(visited._height); ++y)
    for (int x = 0; x<(int)(visited._width); ++x)
      {
        if ((label = image (x, y)) && !visited (x, y))
          {
            Pixel  *p = g_new (Pixel, 1);
            size_t  regionSize = 0;

            p->x = x;
            p->y = y;
            g_queue_push_tail (q, p);
            visited (x, y) = true;

            while (! g_queue_is_empty (q))
              {
                Pixel *p = (Pixel *) g_queue_pop_head (q);

                if (image.containsXYZC (p->x + 1, p->y) &&
                    (label == image (p->x + 1, p->y))   &&
                    ! visited (p->x + 1, p->y))
                  {
                    Pixel *p2 = g_new (Pixel, 1);

                    p2->x = p->x + 1;
                    p2->y = p->y;
                    g_queue_push_tail (q, p2);
                    visited (p->x + 1, p->y) = true;
                  }
                if (image.containsXYZC (p->x - 1, p->y) &&
                    (label == image (p->x - 1, p->y))   &&
                    ! visited (p->x - 1, p->y))
                  {
                    Pixel *p2 = g_new (Pixel, 1);

                    p2->x = p->x - 1;
                    p2->y = p->y;
                    g_queue_push_tail (q, p2);
                    visited (p->x - 1, p->y) = true;
                  }
                if (image.containsXYZC (p->x, p->y - 1) &&
                    (label == image (p->x, p->y - 1))   &&
                    ! visited (p->x, p->y - 1))
                  {
                    Pixel *p2 = g_new (Pixel, 1);

                    p2->x = p->x;
                    p2->y = p->y - 1;
                    g_queue_push_tail (q, p2);
                    visited (p->x, p->y - 1) = true;
                  }
                if (image.containsXYZC (p->x, p->y + 1) &&
                    (label == image (p->x, p->y + 1))   &&
                    ! visited (p->x, p->y + 1))
                  {
                    Pixel *p2 = g_new (Pixel, 1);

                    p2->x = p->x;
                    p2->y = p->y + 1;
                    g_queue_push_tail (q, p2);
                    visited (p->x, p->y + 1) = true;
                  }
                if (image.containsXYZC (p->x + 1, p->y + 1) &&
                    (label == image (p->x + 1, p->y + 1))   &&
                    ! visited (p->x + 1, p->y + 1))
                  {
                    Pixel *p2 = g_new (Pixel, 1);

                    p2->x = p->x + 1;
                    p2->y = p->y + 1;
                    g_queue_push_tail (q, p2);
                    visited (p->x + 1, p->y + 1) = true;
                  }
                if (image.containsXYZC (p->x - 1, p->y - 1) &&
                    (label == image (p->x - 1, p->y - 1)) &&
                    ! visited (p->x - 1, p->y - 1))
                  {
                    Pixel *p2 = g_new (Pixel, 1);

                    p2->x = p->x - 1;
                    p2->y = p->y - 1;
                    g_queue_push_tail (q, p2);
                    visited (p->x - 1, p->y - 1) = true;
                  }
                if (image.containsXYZC (p->x - 1, p->y + 1) &&
                    (label == image (p->x - 1, p->y + 1))   &&
                    ! visited (p->x - 1, p->y + 1))
                  {
                    Pixel *p2 = g_new (Pixel, 1);

                    p2->x = p->x - 1;
                    p2->y = p->y + 1;
                    g_queue_push_tail (q, p2);
                    visited (p->x - 1, p->y + 1) = true;
                  }
                if (image.containsXYZC (p->x + 1, p->y - 1) &&
                    (label == image (p->x + 1, p->y - 1))   &&
                    ! visited (p->x + 1, p->y - 1))
                  {
                    Pixel *p2 = g_new (Pixel, 1);

                    p2->x = p->x + 1;
                    p2->y = p->y - 1;
                    g_queue_push_tail (q, p2);
                    visited (p->x + 1, p->y - 1) = true;
                  }

                ++regionSize;
                if (regionSize <= size)
                  g_array_append_val (region, *p);
                g_free (p);
              }
            if (regionSize <= size)
              {
                Pixel *pixel = (Pixel *) region->data;
                gint   i = 0;

                for (; i < region->len; i++)
                  {
                    image (pixel->x, pixel->y) = 0;
                    pixel++;
                  }
              }
            g_array_remove_range (region, 0, region->len);
          }
      }
  g_array_free (region, TRUE);
  g_queue_free_full (q, g_free);
}

void
removeSmallLabeledRegions (GeglBuffer   *image,
                           unsigned int  size)
{
  gint      width  = gegl_buffer_get_width (image);
  gint      height = gegl_buffer_get_height (image);
  gboolean  visited[width][height] = { 0 };
  GQueue   *q = g_queue_new ();
  GArray   *region;
  guint32   label;

  region = g_array_sized_new (FALSE, TRUE, sizeof (Pixel), size);

  for (int y = 0; y < height; ++y)
    for (int x = 0; x < width; ++x)
      {
        guint32 l;

        gegl_buffer_sample (image, x, y, NULL, &l, NULL,
                            GEGL_SAMPLER_NEAREST, GEGL_ABYSS_NONE);
        if ((label = l) && ! visited[x][y])
          {
            Pixel  *p = g_new (Pixel, 1);
            size_t  regionSize = 0;

            p->x = x;
            p->y = y;
            g_queue_push_tail (q, p);
            visited[x][y] = TRUE;

            while (! g_queue_is_empty (q))
              {
                Pixel *p = (Pixel *) g_queue_pop_head (q);
                gint   p2x;
                gint   p2y;

                p2x = p->x + 1;
                p2y = p->y;
                gegl_buffer_sample (image, p2x, p2y, NULL, &l, NULL,
                                    GEGL_SAMPLER_NEAREST, GEGL_ABYSS_NONE);
                if (p2x >= 0 && p2x < width  &&
                    p2y >= 0 && p2y < height &&
                    label == l && ! visited[p2x][p2y])
                  {
                    Pixel *p2 = g_new (Pixel, 1);

                    p2->x = p2x;
                    p2->y = p2y;
                    g_queue_push_tail (q, p2);
                    visited[p2x][p2y] = TRUE;
                  }
                p2x = p->x - 1;
                p2y = p->y;
                gegl_buffer_sample (image, p2x, p2y, NULL, &l, NULL,
                                    GEGL_SAMPLER_NEAREST, GEGL_ABYSS_NONE);
                if (p2x >= 0 && p2x < width  &&
                    p2y >= 0 && p2y < height &&
                    label == l && ! visited[p2x][p2y])
                  {
                    Pixel *p2 = g_new (Pixel, 1);

                    p2->x = p2x;
                    p2->y = p2y;
                    g_queue_push_tail (q, p2);
                    visited[p2x][p2y] = TRUE;
                  }
                p2x = p->x;
                p2y = p->y - 1;
                gegl_buffer_sample (image, p2x, p2y, NULL, &l, NULL,
                                    GEGL_SAMPLER_NEAREST, GEGL_ABYSS_NONE);
                if (p2x >= 0 && p2x < width  &&
                    p2y >= 0 && p2y < height &&
                    label == l && ! visited[p2x][p2y])
                  {
                    Pixel *p2 = g_new (Pixel, 1);

                    p2->x = p2x;
                    p2->y = p2y;
                    g_queue_push_tail (q, p2);
                    visited[p2x][p2y] = TRUE;
                  }
                p2x = p->x;
                p2y = p->y + 1;
                gegl_buffer_sample (image, p2x, p2y, NULL, &l, NULL,
                                    GEGL_SAMPLER_NEAREST, GEGL_ABYSS_NONE);
                if (p2x >= 0 && p2x < width  &&
                    p2y >= 0 && p2y < height &&
                    label == l && ! visited[p2x][p2y])
                  {
                    Pixel *p2 = g_new (Pixel, 1);

                    p2->x = p2x;
                    p2->y = p2y;
                    g_queue_push_tail (q, p2);
                    visited[p2x][p2y] = TRUE;
                  }
                p2x = p->x + 1;
                p2y = p->y + 1;
                gegl_buffer_sample (image, p2x, p2y, NULL, &l, NULL,
                                    GEGL_SAMPLER_NEAREST, GEGL_ABYSS_NONE);
                if (p2x >= 0 && p2x < width  &&
                    p2y >= 0 && p2y < height &&
                    label == l && ! visited[p2x][p2y])
                  {
                    Pixel *p2 = g_new (Pixel, 1);

                    p2->x = p2x;
                    p2->y = p2y;
                    g_queue_push_tail (q, p2);
                    visited[p2x][p2y] = TRUE;
                  }
                p2x = p->x - 1;
                p2y = p->y - 1;
                gegl_buffer_sample (image, p2x, p2y, NULL, &l, NULL,
                                    GEGL_SAMPLER_NEAREST, GEGL_ABYSS_NONE);
                if (p2x >= 0 && p2x < width  &&
                    p2y >= 0 && p2y < height &&
                    label == l && ! visited[p2x][p2y])
                  {
                    Pixel *p2 = g_new (Pixel, 1);

                    p2->x = p2x;
                    p2->y = p2y;
                    g_queue_push_tail (q, p2);
                    visited[p2x][p2y] = TRUE;
                  }
                p2x = p->x - 1;
                p2y = p->y + 1;
                gegl_buffer_sample (image, p2x, p2y, NULL, &l, NULL,
                                    GEGL_SAMPLER_NEAREST, GEGL_ABYSS_NONE);
                if (p2x >= 0 && p2x < width  &&
                    p2y >= 0 && p2y < height &&
                    label == l && ! visited[p2x][p2y])
                  {
                    Pixel *p2 = g_new (Pixel, 1);

                    p2->x = p2x;
                    p2->y = p2y;
                    g_queue_push_tail (q, p2);
                    visited[p2x][p2y] = TRUE;
                  }
                p2x = p->x + 1;
                p2y = p->y - 1;
                gegl_buffer_sample (image, p2x, p2y, NULL, &l, NULL,
                                    GEGL_SAMPLER_NEAREST, GEGL_ABYSS_NONE);
                if (p2x >= 0 && p2x < width  &&
                    p2y >= 0 && p2y < height &&
                    label == l && ! visited[p2x][p2y])
                  {
                    Pixel *p2 = g_new (Pixel, 1);

                    p2->x = p2x;
                    p2->y = p2y;
                    g_queue_push_tail (q, p2);
                    visited[p2x][p2y] = TRUE;
                  }

                ++regionSize;
                if (regionSize <= size)
                  g_array_append_val (region, *p);
                g_free (p);
              }
            if (regionSize <= size)
              {
                Pixel *pixel = (Pixel *) region->data;
                gint   i = 0;

                for (; i < region->len; i++)
                  {
                    l = 0;
                    gegl_buffer_set (image, GEGL_RECTANGLE (pixel->x, pixel->y, 1, 1), 0,
                                     NULL, &l, GEGL_AUTO_ROWSTRIDE);
                    pixel++;
                  }
              }
            g_array_remove_range (region, 0, region->len);
          }
      }
  g_array_free (region, TRUE);
  g_queue_free_full (q, g_free);
}

static void
keep8ConnectedRegionsWithSignificantArea (cimg_library::CImg<uchar> *image,
                                          int                        minimumArea)
{
  // TODO
  cimg_library::CImg<bool>  visited;
  GQueue                   *q = g_queue_new ();
  GArray                   *region;

  visited.assign (image->width (), image->height ());
  memset (visited.data (), 0, visited.size () * sizeof (bool));

  region = g_array_sized_new (TRUE, TRUE, sizeof (Pixel *), minimumArea);

  for (int y = 0; y < (int) (visited._height); ++y)
    for (int x = 0; x < (int) (visited._width); ++x)
      {
        if ((*image) (x, y) && !visited (x, y))
          {
            Pixel *p = g_new (Pixel, 1);
            int    regionSize = 0;

            p->x = x;
            p->y = y;

            g_queue_push_tail (q, p);
            visited (x, y) = true;

            while (! g_queue_is_empty (q))
              {
                Pixel *p = (Pixel *) g_queue_pop_head (q);

                if (image->containsXYZC (p->x + 1, p->y) &&
                    (*image) (p->x + 1, p->y) && !visited (p->x + 1, p->y))
                  {
                    Pixel *p2 = g_new (Pixel, 1);

                    p2->x = p->x + 1;
                    p2->y = p->y;
                    g_queue_push_tail (q, p2);
                    visited (p->x + 1, p->y) = true;
                  }
                if (image->containsXYZC (p->x - 1, p->y) &&
                    (*image) (p->x - 1, p->y) && !visited (p->x - 1, p->y))
                  {
                    Pixel *p2 = g_new (Pixel, 1);

                    p2->x = p->x - 1;
                    p2->y = p->y;
                    g_queue_push_tail (q, p2);
                    visited (p->x - 1, p->y) = true;
                  }
                if (image->containsXYZC (p->x, p->y - 1) &&
                    (*image) (p->x, p->y - 1) && !visited (p->x, p->y - 1))
                  {
                    Pixel *p2 = g_new (Pixel, 1);

                    p2->x = p->x;
                    p2->y = p->y - 1;
                    g_queue_push_tail (q, p2);
                    visited (p->x, p->y - 1) = true;
                  }
                if (image->containsXYZC (p->x, p->y + 1) &&
                    (*image) (p->x, p->y + 1) && !visited (p->x, p->y + 1))
                  {
                    Pixel *p2 = g_new (Pixel, 1);

                    p2->x = p->x;
                    p2->y = p->y + 1;
                    g_queue_push_tail (q, p2);
                    visited (p->x, p->y + 1) = true;
                  }
                if (image->containsXYZC (p->x + 1, p->y + 1) &&
                    (*image) (p->x + 1, p->y + 1) && !visited (p->x + 1, p->y + 1))
                  {
                    Pixel *p2 = g_new (Pixel, 1);

                    p2->x = p->x + 1;
                    p2->y = p->y + 1;
                    g_queue_push_tail (q, p2);
                    visited (p->x + 1, p->y + 1) = true;
                  }
                if (image->containsXYZC (p->x - 1, p->y - 1) &&
                    (*image) (p->x - 1, p->y - 1) && !visited (p->x - 1, p->y - 1))
                  {
                    Pixel *p2 = g_new (Pixel, 1);

                    p2->x = p->x - 1;
                    p2->y = p->y - 1;
                    g_queue_push_tail (q, p2);
                    visited (p->x - 1, p->y - 1) = true;
                  }
                if (image->containsXYZC (p->x - 1, p->y + 1) &&
                    (*image) (p->x - 1, p->y + 1) && !visited (p->x - 1, p->y + 1))
                  {
                    Pixel *p2 = g_new (Pixel, 1);

                    p2->x = p->x - 1;
                    p2->y = p->y + 1;
                    g_queue_push_tail (q, p2);
                    visited (p->x - 1, p->y + 1) = true;
                  }
                if (image->containsXYZC (p->x + 1, p->y - 1) &&
                    (*image) (p->x + 1, p->y - 1) && !visited (p->x + 1, p->y - 1))
                  {
                    Pixel *p2 = g_new (Pixel, 1);

                    p2->x = p->x + 1;
                    p2->y = p->y - 1;
                    g_queue_push_tail (q, p2);
                    visited (p->x + 1, p->y - 1) = true;
                  }

                ++regionSize;
                if (regionSize < minimumArea)
                  g_array_append_val (region, *p);
                g_free (p);
              }
            if (regionSize < minimumArea)
              {
                Pixel *pixel = (Pixel *) region->data;
                gint   i = 0;

                for (; i < region->len; i++)
                  {
                    (*image) (pixel->x, pixel->y) = 0;
                    pixel++;
                  }
              }
            g_array_remove_range (region, 0, region->len);
          }
      }
  g_array_free (region, TRUE);
  g_queue_free_full (q, g_free);
}

static int
cimg_numberOfTransitions (GArray                          *pixels,
                          const cimg_library::CImg<uchar> *image,
                          GeglBuffer                      *buffer,
                          gboolean                         borderValue)
{
  int result = 0;

  if (pixels->len > 0)
    {
      Pixel    it = g_array_index (pixels, Pixel, 0);
      guchar   value;
      gboolean  previous;
      gboolean _previous;
      gint     i;

      previous = (*image) (it.x, it.y);
      gegl_buffer_sample (buffer, (gint) it.x, (gint) it.y, NULL, &value, NULL,
                          GEGL_SAMPLER_NEAREST, GEGL_ABYSS_NONE);
      _previous = (gboolean) value;
      gimp_assert (previous == _previous);

      /* Starts at the second element. */
      for (i = 1; i < pixels->len; i++)
        {
          gboolean val;

          it = g_array_index (pixels, Pixel, i);
          if (image->containsXYZC (it.x, it.y))
            {
              gimp_assert (it.x >= 0 && it.x < gegl_buffer_get_width (buffer) &&
                           it.y >= 0 && it.y < gegl_buffer_get_height (buffer));
              val = (*image) (it.x, it.y);
            }
          else
            {
              gimp_assert (it.x < 0 || it.x >= gegl_buffer_get_width (buffer) ||
                           it.y < 0 || it.y >= gegl_buffer_get_height (buffer));
              val = borderValue;
            }
          result += (val != previous);
          previous = val;
        }
    }

  return result;
}

GArray *
cimg_lineSegmentUntilHit (const cimg_library::CImg<uchar> & mask,
                          Pixel                             start,
                          GimpVector2                       direction,
                          int                               size)
{
  bool         out = false;
  GArray      *points = g_array_new (FALSE, TRUE, sizeof (Pixel));
  int          tmax;
  GimpVector2  p0 = gimp_vector2_new (start.x, start.y);

  gimp_vector2_mul (&direction, (gdouble) size);
  direction.x = round (direction.x);
  direction.y = round (direction.y);

  tmax = MAX (abs ((int) direction.x), abs ((int) direction.y));

  for (int t = 0; t <= tmax; ++t)
    {
      GimpVector2 v = gimp_vector2_add_val (p0, gimp_vector2_mul_val (direction, (float)t / tmax));
      Pixel       p;

      p.x = (gint) round (v.x);
      p.y = (gint) round (v.y);
      if (mask.containsXYZC (p.x, p.y))
        {
          uchar value = mask (p.x, p.y);
          if (out && value)
            {
              return points;
            }
          out = !value;
        }
      else if (out)
        {
          return points;
        }
      else
        {
          g_array_free (points, TRUE);
          return g_array_new (FALSE, TRUE, sizeof (Pixel));
        }
      g_array_append_val (points, p);
    }

  g_array_free (points, TRUE);
  return g_array_new (FALSE, TRUE, sizeof (Pixel));
}

guint32 *
get_histogram (GeglBuffer    *labels,
               const guint32  nb_levels,
               const guint32  min_value,
               const guint32  max_value)
{
  GeglBufferIterator *gi;
  guint32            *res = NULL;
  const guint32       vmin = MIN (min_value, max_value);
  const guint32       vmax = MAX (min_value, max_value);

  if (! nb_levels)
    return res;

  res = g_new (guint32, nb_levels);
  memset (res, 0, sizeof (guint32) * nb_levels);

  gi = gegl_buffer_iterator_new (labels, NULL, 0, babl_format ("Y u32"),
                                 GEGL_ACCESS_READ, GEGL_ABYSS_NONE, 1);
  while (gegl_buffer_iterator_next (gi))
    {
      guint32 *data = (guint32*) gi->items[0].data;
      gint     k;

      for (k = 0; k < gi->length; k++)
        {
          if (data[0] >= vmin && data[0] <= vmax)
            res[data[0] == vmax ? nb_levels - 1 : (guint32) ((gdouble) (data[0] - vmin) * nb_levels / (gdouble) (vmax - vmin))] += 1;
          data++;
        }
    }

  return res;
}

void
removeLargestLabel (GeglBuffer *labels)
{
  GeglBufferIterator *gi;
  guint32            *h;
  guint32             m = 0;
  guint32             xMax = 0;

  gi = gegl_buffer_iterator_new (labels, NULL, 0, NULL,
                                 GEGL_ACCESS_READ, GEGL_ABYSS_NONE, 1);
  while (gegl_buffer_iterator_next (gi))
    {
      guint32 *data = (guint32*) gi->items[0].data;
      gint     k;

      for (k = 0; k < gi->length; k++)
        {
          m = MAX (*data, m);
          data += 2;
        }
    }
  h = get_histogram (labels, m, 1, m);

  for (int x = 0;  x < m; ++x)
    {
      if (h[x] > h[xMax])
        {
          xMax = (guint32) x;
        }
    }
  printf("GEGL Max is %d xMax is %d\n", (gint) m, (gint) xMax);
  gi = gegl_buffer_iterator_new (labels, NULL, 0, NULL,
                                 GEGL_ACCESS_READWRITE, GEGL_ABYSS_NONE, 1);
  while (gegl_buffer_iterator_next (gi))
    {
      guint32 *data = (guint32*) gi->items[0].data;
      gint     k;

      for (k = 0; k < gi->length; k++)
        {
          if (data[0] == xMax + 1)
            {
              data[0] = 0;
              data[1] = 0;
            }
          data += 2;
        }
    }
}

void
cimg_removeLargestLabel (cimg_library::CImg<ulong> & labels)
{
  ulong                     m = labels.max ();
  cimg_library::CImg<ulong> h = labels.get_histogram (m, 1, m);
  ulong                     xMax = 0;

  for (int  x = 0;  x<(int)(h._width); ++ x)
    {
      if (h (x) > h (xMax))
        {
          xMax = x;
        }
    }
  printf("CImg Max is %d xMax is %d\n", m, xMax);
  for (ulong *p = (labels)._data, *_maxp = (labels)._data + (labels).size(); p<_maxp; ++p)
    {
      if (*p == xMax + 1)
        {
          *p = 0;
        }
    }
}

gdouble *
toonColorMap (int  size,
              bool zeroIsWhite,
              int  maxSaturation,
              int  minIntensity)
{
  gdouble *cmap = g_new (gdouble, size * 3);
  int      h = 0;

  maxSaturation = MAX (3, maxSaturation);
  minIntensity = MIN (252, minIntensity);
  srand (time (NULL));

  for (int x = 0; x < size; ++x)
    {
      gdouble a, r, g, b;
      gdouble s;
      gdouble i;

      h = h + rand () % 41 + 85;
      h = h % 360;

      /* Saturation */
      s = (rand () % maxSaturation + 1) / 255.0;
      /* Intensity */
      i = (rand () % (minIntensity - 254) + 255.0) / 255.0;

      a = i * (1.0 - s);
      if (h < 120)
        {
          b = a;
          r = (i * (1 + s * cos ((double) h * G_PI / 180) / cos ((double) (60 - h) * G_PI / 180)));
          g = 3 * i - (r + b);
        }
      else if (h < 240)
        {
          h -= 120;
          r = a;
          g = (i * (1 + s * cos ((double) h * G_PI / 180) / cos ((double) (60 - h) * G_PI / 180)));
          b = 3 * i - (r + g);
        }
      else
        {
          h -= 240;
          g = a;
          b = (i * (1 + s * cos ((double) h * G_PI / 180)/ cos ((double) (60 - h) * G_PI / 180)));
          r = 3 * i - (g + b);
        }
      cmap[x * 3] = CLAMP (r * 255.0, 0.0, 255.0);
      cmap[x * 3 + 1] = CLAMP (g * 255.0, 0.0, 255.0);
      cmap[x * 3 + 2] = CLAMP (b * 255.0, 0.0, 255.0);
    }

  if (zeroIsWhite)
    {
      cmap[0] = 255.0;
      cmap[1] = 255.0;
      cmap[2] = 255.0;
    }

  return cmap;
}

cimg_library::CImg<uchar>
cimg_toonColorMap (int  size,
                   bool zeroIsWhite,
                   int  maxSaturation,
                   int  minIntensity)
{
  cimg_library::CImg<float> cmap (size, 1, 1, 3);
  int                       h = 0;

  maxSaturation = MAX (3, maxSaturation);
  minIntensity = MIN (252, minIntensity);
  srand (time(NULL));
  for (int  x = 0;  x < (int) (cmap._width); ++x)
    {
      h = h + rand () % 41 + 85;
      if (h > 360)
        {
          h -= 360;
        }
      cmap (x, 0, 0, 0) = h;
      cmap (x, 0, 0, 1) = (rand () % maxSaturation + 1) / 255.0; // Saturation
      cmap (x, 0, 0, 2) = (rand () % (minIntensity - 254) + 255) / 255.0; // Intensity
    }
  cmap.HSItoRGB ();
  if (zeroIsWhite)
    {
      cmap (0, 0, 0, 0) = 255;
      cmap (0, 0, 0, 1) = 255;
      cmap (0, 0, 0, 2) = 255;
    }
  return cmap;
}

cimg_library::CImg<uchar>
superimposeGrayImage (cimg_library::CImg<uchar>       & lineart,
                      const cimg_library::CImg<uchar> & image)
{
  cimg_library::CImg<uchar> overlay = lineart;
  cimg_library::CImg<uchar> background = image;
  float                     sv, dv, sa, da, a;

  gimp_assert (image.spectrum () == 1 || image.spectrum () == 3);
  gimp_assert (lineart.spectrum () <= 4);

  if (cimg_significantAlphaChannel (lineart, NULL))
    {
      if (overlay.spectrum () == 2)
        {
          cimg_library::CImg<uchar> alpha = overlay.get_channel (1);
          cimg_library::CImg<uchar> overlay = overlay.get_channel (0).resize (-100, -100, 1, 3, 1).append (alpha, 'x');
        }
    }
  else
    {
      overlay.resize (-100, -100, 1, 4, 1);
      for (int y = 0; y<(int)(overlay._height); ++y)
        for (int x = 0; x<(int)(overlay._width); ++x)
          { overlay (x, y, 0, 3) = 255 - overlay (x, y, 0, 0); }
    }

  if (background.spectrum () == 1)
    {
      background.resize (-100, -100, 1, 3, 1);
    }
  background.resize (-100, -100, 1, 4, 0);
  memset (background.data () + (background.width () * background.height () * (background.spectrum () - 1)), 255, background.width () * background.height ());

  //  A = srcA + dstA (1 - srcA)
  //  R = (srcR * srcA + dstR * dstA * (1 - srcA)) / A
  //  G = (srcG * srcA + dstG * dstA * (1 - srcA)) / A
  //  B = (srcB * srcA + dstB * dstA * (1 - srcA)) / A
  for (int y = 0; y<(int)(background._height); ++y)
    for (int x = 0; x<(int)(background._width); ++x)
      {
        sa = overlay (x, y, 0, 3) / 255.0;
        da = background (x, y, 0, 3) / 255.0;
        a = background (x, y, 0, 3) = (sa + da * (1 - sa));

        sv = overlay (x, y, 0, 0) / 255.0;
        dv = background (x, y, 0, 0) / 255.0;
        background (x, y, 0, 0) = (unsigned char) MIN (255.0f, 255 * ((sv * sa + dv * da * (1.0f - sa)) / a));

        sv = overlay (x, y, 0, 1) / 255.0;
        dv = background (x, y, 0, 1) / 255.0;
        background (x, y, 0, 1) = (unsigned char) MIN (255.0f, 255 * ((sv * sa + dv * da * (1.0f - sa)) / a));

        sv = overlay (x, y, 0, 2) / 255.0;
        dv = background (x, y, 0, 2) / 255.0;
        background (x, y, 0, 2) = (unsigned char) MIN (255.0f, 255 * ((sv * sa + dv * da * (1.0f - sa)) / a));

        background (x, y, 0, 3) = (unsigned char) MIN (255.0f, a * 255);
      }
  return background;
}

static gboolean
gimp_pixel_array_are_same (GArray *set1,
                           GArray *set2)
{
  gint i;

  if (set1->len != set2->len)
    return FALSE;

  for (i = 0; i < set1->len; i++)
    {
      Pixel it1 = g_array_index (set1, Pixel, i);
      Pixel it2 = g_array_index (set2, Pixel, i);

      if (it1.x != it2.x || it1.y != it2.y)
        return FALSE;
    }
  return TRUE;
}

static gboolean
gimp_candidates_are_same (GList *list1,
                          GList *list2)
{
  GList *iter1;
  GList *iter2;

  if (g_list_length (list1) != g_list_length (list2))
    {
      printf("ERROR: candidates have different size (%d vs %d)!\n",
             g_list_length (list1), g_list_length (list2));
    return FALSE;
    }

  for (iter1 = list1, iter2 = list2; iter1; iter1 = iter1->next, iter2 = iter2->next)
    {
      SplineCandidate *candidate1 = (SplineCandidate *) iter1->data;
      SplineCandidate *candidate2 = (SplineCandidate *) iter2->data;

      if (candidate1->p1.x != candidate2->p1.x ||
          candidate1->p1.y != candidate2->p1.y ||
          candidate1->p2.x != candidate2->p2.x ||
          candidate1->p2.y != candidate2->p2.y ||
          candidate1->quality != candidate2->quality)
        return FALSE;
    }
  return TRUE;
}
