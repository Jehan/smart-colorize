/**
 *  @file EdgelSet.cpp
 *
 *  Copyright 2017 S�bastien Fourey & David Tchumperl�
 *
 *  This file is part of "Smart-Colorize", a C++ implementation
 *  of the lineart semi-automatic colorization method.
 *
 *  Smart-Colorize is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Smart-Colorize is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Smart-Colorize.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <gmodule.h>
extern "C" {
#include <gegl.h>

#include "../libgimpmath/gimpvector.h"
#include "gimplineart.h"
}

#include "EdgelSet.h"

/**** TEMPORARY: remove after GEGL-reimplementation ***/
static void      cimg_gimp_edgelset_next8      (const cimg_library::CImg<unsigned char> *image,
                                                Edgel                                   *pe,
                                                Edgel                                   *n);
static void      cimg_gimp_edgelset_buildGraph (GArray                                  *set,
                                                const cimg_library::CImg<unsigned char> *image,
                                                GHashTable                              *edgel2index);
/**** END TEMPORARY ***/

/* Edgel private functions */

/* Edgel set public functions */

gboolean
gimp_edgelset_are_same (GArray *set1,
                        GArray *set2)
{
  gint i;

  if (set1->len != set2->len)
    return FALSE;

  for (i = 0; i < set1->len; i++)
    {
      Edgel *it1 = g_array_index (set1, Edgel*, i);
      Edgel *it2 = g_array_index (set2, Edgel*, i);

      /* I don't use gimp_edgel_cmp() as I do more complete comparison. */
      if (it1->x != it2->x || it1->y != it2->y ||
          it1->direction != it2->direction     ||
          it1->x_normal != it2->x_normal       ||
          it1->y_normal != it2->y_normal       ||
          it1->curvature != it2->curvature     ||
          it1->next != it2->next               ||
          it1->previous != it2->previous)
        return FALSE;
    }
  return TRUE;
}

/* Edgel set private functions */

/**** TEMPORARY: remove after GEGL-reimplementation ***/

GArray *
cimg_gimp_edgelset_new (const cimg_library::CImg<unsigned char> *image)
{
  GArray     *set;
  GHashTable *edgel2index;
  int         lx = image->width () - 1;
  int         ly = image->height () - 1;

  set = g_array_new (TRUE, TRUE, sizeof (Edgel *));
  g_array_set_clear_func (set, (GDestroyNotify) gimp_edgel_clear);

  edgel2index = g_hash_table_new ((GHashFunc) edgel2index_hash_fun,
                                  (GEqualFunc) edgel2index_equal_fun);

  for (int y = 0; y < (int) image->height (); ++y)
    for (int x = 0; x < (int) image->width (); ++x)
      {
        if ((*image) (x, y))
          {
            if (!y || ! (*image) (x, y - 1))
              {
                gimp_edgelset_add (set, x, y, YMinusDirection, edgel2index);
              }
            if ((y == ly) || ! (*image) (x, y + 1))
              {
                gimp_edgelset_add (set, x, y, YPlusDirection, edgel2index);
              }
            if (!x || ! (*image) (x - 1, y))
              {
                gimp_edgelset_add (set, x, y, XMinusDirection, edgel2index);
              }
            if ((x == lx) || ! (*image) (x + 1, y))
              {
                gimp_edgelset_add (set, x, y, XPlusDirection, edgel2index);
              }
          }
      }
  cimg_gimp_edgelset_buildGraph (set, image, edgel2index);
  g_hash_table_destroy (edgel2index);
  gimp_edgelset_init_normals (set);

  return set;
}

static void
cimg_gimp_edgelset_next8 (const cimg_library::CImg<unsigned char> *image,
                          Edgel                                   *it,
                          Edgel                                   *n)
{
  const int lx = image->width () - 1;
  const int ly = image->height () - 1;

  n->x = it->x;
  n->y = it->y;
  n->direction = it->direction;
  switch (n->direction)
    {
    case XPlusDirection:
      if ((n->x != lx) && (n->y != ly) && (*image) (n->x + 1, n->y + 1))
        {
          ++(n->y);
          ++(n->x);
          n->direction = YMinusDirection;
        }
      else if ((n->y != ly) && (*image) (n->x, n->y + 1))
        {
          ++(n->y);
        }
      else
        {
          n->direction = YPlusDirection;
        }
      break;
    case YMinusDirection:
      if ((n->x != lx) && n->y && (*image) (n->x + 1, n->y - 1))
        {
          ++(n->x);
          --(n->y);
          n->direction = XMinusDirection;
        }
      else if ((n->x != lx) && (*image) (n->x + 1, n->y))
        {
          ++(n->x);
        }
      else
        {
          n->direction = XPlusDirection;
        }
      break;
    case XMinusDirection:
      if (n->x && n->y && (*image) (n->x - 1, n->y - 1))
        {
          --(n->x);
          --(n->y);
          n->direction = YPlusDirection;
        }
      else if (n->y && (*image) (n->x, n->y - 1))
        {
          --(n->y);
        }
      else
        {
          n->direction = YMinusDirection;
        }
      break;
    case YPlusDirection:
      if (n->x && (n->y != ly) && (*image) (n->x - 1, n->y + 1))
        {
          --(n->x);
          ++(n->y);
          n->direction = XPlusDirection;
        }
      else if (n->x && (*image) (n->x - 1, n->y))
        {
          --(n->x);
        }
      else
        {
          n->direction = XMinusDirection;
        }
      break;
    default:
      gimp_assert (false);
      break;
    }
}

static void
cimg_gimp_edgelset_buildGraph (GArray                                  *set,
                               const cimg_library::CImg<unsigned char> *image,
                               GHashTable                              *edgel2index)
{
  Edgel edgel;
  gint  i;

  for (i = 0; i < set->len; i++)
    {
      Edgel *neighbor;
      guint  neighbor_pos;
      Edgel *it = g_array_index (set, Edgel *, i);

      cimg_gimp_edgelset_next8 (image, it, &edgel);

      gimp_assert (g_hash_table_contains (edgel2index, &edgel));
      neighbor_pos = GPOINTER_TO_UINT (g_hash_table_lookup (edgel2index, &edgel));
      it->next = neighbor_pos;
      neighbor = g_array_index (set, Edgel *, neighbor_pos);
      neighbor->previous = i;
    }
}

long
cimg_trackAndMark (cimg_library::CImg<unsigned char> *mask,
                   Edgel                              edgel,
                   long                               sizeLimit)
{
  Edgel start = edgel;
  long  count = 1;

  do
    {
      cimg_gimp_edgelset_next8 (mask, &edgel, &edgel);
      if ((*mask) (edgel.x, edgel.y) & 2)
        { // Only mark pixels of the spline/segment
          if ((*mask) (edgel.x, edgel.y) & (4 << edgel.direction))
            {
              return -1;
            }
          (*mask) (edgel.x, edgel.y) |= (4 << edgel.direction); // Mark edgel in pixel (1 == In Mask, 2 == Spline/Segment)
        }
      if (gimp_edgel_cmp (&edgel, &start) != 0)
        {
          ++count;
        }
    } while (gimp_edgel_cmp (&edgel, &start) != 0 && count <= sizeLimit);
  return count;
}

long
cimg_regionArea (const cimg_library::CImg<unsigned char> *mask,
                 Edgel                                    startingEdgel)
{
  Edgel edgel = startingEdgel;
  long  area = 0;

  do
    {
      if (edgel.direction == XPlusDirection)
        {
          area += edgel.x;
        }
      else if (edgel.direction == XMinusDirection)
        {
          area -= edgel.x - 1;
        }
      cimg_gimp_edgelset_next8 (mask, &edgel, &edgel);
    } while (gimp_edgel_cmp (&edgel, &startingEdgel) != 0);

  return area;
}

/**** END TEMPORARY ***/
