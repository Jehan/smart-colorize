/* GIMP - The GNU Image Manipulation Program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * Copyright (C) 2017 Sébastien Fourey & David Tchumperlé
 * Copyright (C) 2018 Jehan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __GIMP_LINEART__
#define __GIMP_LINEART__


GeglBuffer * gimp_lineart_close (GeglBuffer          *line_art,
                                 gboolean             select_transparent,
                                 gfloat               stroke_threshold,
                                 gint                 erosion,
                                 gint                 minimal_lineart_area,
                                 gint                 normal_estimate_mask_size,
                                 gfloat               end_point_rate,
                                 gint                 spline_max_length,
                                 gfloat               spline_max_angle,
                                 gint                 end_point_connectivity,
                                 gfloat               spline_roundness,
                                 gboolean             allow_self_intersections,
                                 gint                 created_regions_significant_area,
                                 gint                 created_regions_minimum_area,
                                 gboolean             small_segments_from_spline_sources,
                                 gint                 segments_max_length);

/* TEMP: also used in LineArt_Tools.cpp, but will have to be made static. */

static int DeltaX[4] = {+1, -1, 0, 0};
static int DeltaY[4] = {0, 0, +1, -1};

typedef GimpVector2 Pixel;
typedef struct _SplineCandidate
{
  Pixel p1;
  Pixel p2;
  float quality;
} SplineCandidate;

GeglBuffer * gimp_get_labels                         (GeglBuffer            *line_art,
                                                     gboolean                is_high_connectivity);
void         gimp_lineart_erode                      (GeglBuffer            *buffer,
                                                      gint                   s);
void         gimp_lineart_denoise                    (GeglBuffer            *buffer,
                                                      int                    size);
void         gimp_lineart_compute_normals_curvatures (GeglBuffer            *mask,
                                                      gfloat              ***normals,
                                                      gfloat               **curvatures,
                                                      int                    normal_estimate_mask_size);
GArray     * gimp_lineart_curvature_extremums        (gfloat               **curvatures,
                                                      gint                   curvatures_width,
                                                      gint                   curvatures_height);
gint         gimp_spline_candidate_cmp               (const SplineCandidate *a,
                                                      const SplineCandidate *b,
                                                      gpointer               user_data);
GList      *  gimp_lineart_find_spline_candidates     (GArray                *max_positions,
                                                       gfloat              ***normals,
                                                       gint                   distance_threshold,
                                                       gfloat                 max_angle_deg);

GArray     * gimp_lineart_discrete_spline            (Pixel                   p0,
                                                      GimpVector2             n0,
                                                      Pixel                   p1,
                                                      GimpVector2             n1);

gint         gimp_number_of_transitions               (GArray                 *pixels,
                                                       GeglBuffer             *buffer,
                                                       gboolean                border_value);
gboolean     gimp_lineart_curve_creates_region        (GeglBuffer             *mask,
                                                       GArray                 *pixels,
                                                       int                     lower_size_limit,
                                                       int                     upper_size_limit);
GArray     * gimp_lineart_line_segment_until_hit     (const GeglBuffer        *buffer,
                                                      Pixel                    start,
                                                      GimpVector2              direction,
                                                      int                      size);
gfloat       gimp_lineart_estimate_stroke_width      (GeglBuffer              *mask);

guint        visited_hash_fun                        (Pixel                   *key);
gboolean     visited_equal_fun                       (Pixel                   *e1,
                                                      Pixel                   *e2);
inline GimpVector2
pair2normal (Pixel     p,
             gfloat ***normals)
{
  return gimp_vector2_new (normals[(gint) p.x][(gint) p.y][0],
                           normals[(gint) p.x][(gint) p.y][1]);
}

typedef enum _Direction
{
  XPlusDirection  = 0,
  XMinusDirection = 1,
  YPlusDirection  = 2,
  YMinusDirection = 3
} Direction;

typedef struct _Edgel
{
  gint      x, y;
  Direction direction;

  gfloat    x_normal;
  gfloat    y_normal;
  gfloat    curvature;
  guint     next, previous;
} Edgel;

void     gimp_edgel_init                 (Edgel             *edgel);
void     gimp_edgel_clear                (Edgel            **edgel);
int      gimp_edgel_cmp                  (const Edgel       *e1,
                                          const Edgel       *e2);
guint    edgel2index_hash_fun            (Edgel              *key);
gboolean edgel2index_equal_fun           (Edgel              *e1,
                                          Edgel              *e2);

glong    gimp_edgel_track_mark           (GeglBuffer         *mask,
                                          Edgel               edgel,
                                          long                size_limit);
glong    gimp_edgel_region_area          (const GeglBuffer   *mask,
                                          Edgel               starting_edgel);

GArray * gimp_edgelset_new              (GeglBuffer          *buffer);
void     gimp_edgelset_add               (GArray             *set,
                                          int                 x,
                                          int                 y,
                                          Direction           direction,
                                          GHashTable         *edgel2index);
void     gimp_edgelset_init_normals      (GArray             *set);
void     gimp_edgelset_smooth_normals    (GArray             *set,
                                          int                 mask_size);
void     gimp_edgelset_compute_curvature (GArray             *set);

#endif /* __GIMP_LINEART__ */
