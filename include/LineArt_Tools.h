/**
 *  @file LineArt_Tools.h
 *
 *  Copyright 2017 S�bastien Fourey & David Tchumperl�
 *
 *  This file is part of "Smart-Colorize", a C++ implementation
 *  of the lineart semi-automatic colorization method.
 *
 *  Smart-Colorize is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Smart-Colorize is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Smart-Colorize.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
#ifndef SMART_COLORIZE_LINEART_TOOLS_H
#define SMART_COLORIZE_LINEART_TOOLS_H

extern "C" {
#include <gegl.h>
}
#include "CImg.h"
#include "../src/gimplineart.h"
#include "EdgelSet.h"

typedef unsigned char uchar;
typedef unsigned long ulong;

struct SmartColorizeParams
{
  int normalEstimateMaskSize;
  int erosion;
  int maximalColorSaturation;
  int minimalColorIntensity;
  gfloat contourDetectionLevel;
  float endPointRate;
  int minimalLineArtArea;
  int splineMaxLength;
  float splineMaxAngle;
  float splineRoundness;
  int segmentsMaxLength;
  int minimalRegionSize;
  int endPointConnectivity;
  bool allowSelfIntersections;
  bool removeLargestLabel;
  int createdRegionsMinimumArea;
  bool smallSegmentsFromSplineSources;
  int createdRegionsSignificantArea;
};

/**
 * @brief closeLineArt Close a lineart by adding splines and line segments
 * @param lineArt[in] A lineart image
 * @param params[in] Algorithm parameters
 * @param closed[out] A binary image with splines and segments added
 */
GeglBuffer * closeLineArt (const cimg_library::CImg<uchar> &lineArt,
                           GeglBuffer                      *buffer,
                           SmartColorizeParams              params,
                           cimg_library::CImg<uchar>       &closed);
     /**
      * @brief autoColorLayer Create an image of regions with random colors, to be used
 *                       as an underlying layer for a given binary lineart.
 * @param lineArt[in] A binary lineart image
 * @param params[in] Algorithm parameters
 * @param colorLayer[out] An image of colorized regions
 */
GeglBuffer * autoColorLayer (const cimg_library::CImg<uchar> &lineArt,
                             GeglBuffer                      *line_art,
                             SmartColorizeParams              params,
                             cimg_library::CImg<uchar>       &colorLayer,
                             GeglBuffer                     **color_layer);

/**
 * @brief colorLayerFromColorSpots
 *
 * @param lineArt[in] A binary lineart image
 * @param spots[in] An image of color spots
 * @param colorLayer[out] An image of colorized regions
 */
void colorLayerFromColorSpots (const cimg_library::CImg<uchar> & lineArt,
                               GeglBuffer *line_art,
                               const cimg_library::CImg<uchar> & spots,
                               GeglBuffer *color_spots,
                               cimg_library::CImg<uchar> & colorLayer);


bool cimg_significantAlphaChannel (const cimg_library::CImg<float> &image,
                                   GeglBuffer                      *buffer);

inline float
strangeNorm (float r, float g, float b)
{
  const float gp = g + 0.3;
  const float bp = b + 0.6;
  return sqrt (r * r + gp * gp + bp * bp);
}

/* TODO: remove with GEGL implementation. */

template<typename T>
gboolean compare_cimg_gegl_buffers (const cimg_library::CImg<T> &cimg_buffer,
                                    GeglBuffer                  *gegl_buffer,
                                    const gchar                 *info);
template<typename T>
cimg_library::CImg<T> convert_gegl_to_cimg (const GeglBuffer *mask);
GArray * cimg_curvatureExtremums  (cimg_library::CImg<float>       &curvatures);
void     rgb2luminance            (cimg_library::CImg<float>       &img);
GArray * cimg_lineSegmentUntilHit (const cimg_library::CImg<uchar> &mask,
                                   Pixel                            start,
                                   GimpVector2                      direction,
                                   int                              size);
/* END TODO */


void cimg_removeLargestLabel (cimg_library::CImg<ulong> & labels);
void removeLargestLabel (GeglBuffer *labels);

void removeSmallLabeledRegions (GeglBuffer   *image,
                                unsigned int  size);
void cimg_removeSmallLabeledRegions (cimg_library::CImg<ulong> &image,
                                     unsigned int               size);

cimg_library::CImg<uchar> superimposeGrayImage (cimg_library::CImg<uchar> & lineart, const cimg_library::CImg<uchar> & image);


cimg_library::CImg<uchar> cimg_toonColorMap (int size, bool zeroIsWhite, int maxSaturation, int minIntensity);
gdouble * toonColorMap (int  size,
                        bool zeroIsWhite,
                        int  maxSaturation,
                        int  minIntensity);

#endif // SMART_COLORIZE_LINEART_TOOLS_H
