/**
 *  @file EdgelSet.h
 *
 *  Copyright 2017 S�bastien Fourey & David Tchumperl�
 *
 *  This file is part of "Smart-Colorize", a C++ implementation
 *  of the lineart semi-automatic colorization method.
 *
 *  Smart-Colorize is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Smart-Colorize is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Smart-Colorize.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
#ifndef SMART_COLORIZE_EDGELSET_H
#define SMART_COLORIZE_EDGELSET_H

#include <gmodule.h>
extern "C" {
#include <gegl.h>
#include "../src/gimplineart.h"
}

#include "CImg.h"

/**** TEMPORARY: remove after GEGL-reimplementation ***/
gboolean gimp_edgelset_are_same          (GArray *set1,
                                          GArray *set2);
GArray * cimg_gimp_edgelset_new          (const cimg_library::CImg<unsigned char> *image);
long     cimg_trackAndMark               (cimg_library::CImg<unsigned char>       *mask,
                                          Edgel                                    edgel,
                                          long                                     sizeLimit);
long     cimg_regionArea                 (const cimg_library::CImg<unsigned char> *mask,
                                          Edgel                                    startingEdgel);
/**** END TEMPORARY ***/

#endif // SMART_COLORIZE_EDGELSET_H
