/* LIBGIMP - The GIMP Library
 * Copyright (C) 1995-1997 Peter Mattis and Spencer Kimball
 *
 * gimpvector.c
 *
 * The gimp_vector* functions were taken from:
 * GCK - The General Convenience Kit
 * Copyright (C) 1996 Tom Bech
 *
 * This library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see
 * <https://www.gnu.org/licenses/>.
 */

/**********************************************/
/* A little collection of useful vector stuff */
/**********************************************/

#include <glib-object.h>

#include "math.h"
#include "gimpvector.h"


/**
 * SECTION: gimpvector
 * @title: GimpVector
 * @short_description: Utilities to set up and manipulate vectors.
 * @see_also: #GimpMatrix2, #GimpMatrix3, #GimpMatrix4
 *
 * Utilities to set up and manipulate vectors.
 **/


/*************************/
/* Some useful constants */
/*************************/

static const GimpVector2 gimp_vector2_zero =   { 0.0, 0.0 };
#if 0
static const GimpVector2 gimp_vector2_unit_x = { 1.0, 0.0 };
static const GimpVector2 gimp_vector2_unit_y = { 0.0, 1.0 };
#endif

/**************************************/
/* Two   dimensional vector functions */
/**************************************/

/**
 * gimp_vector2_new:
 * @x: the X coordinate.
 * @y: the Y coordinate.
 *
 * Creates a #GimpVector2 of coordinates @x and @y.
 *
 * Returns: the resulting #GimpVector2.
 **/
GimpVector2
gimp_vector2_new (gdouble x,
                  gdouble y)
{
  GimpVector2 vector;

  vector.x = x;
  vector.y = y;

  return vector;
}

/**
 * gimp_vector2_set:
 * @vector: a pointer to a #GimpVector2.
 * @x: the X coordinate.
 * @y: the Y coordinate.
 *
 * Sets the X and Y coordinates of @vector to @x and @y.
 **/
void
gimp_vector2_set (GimpVector2 *vector,
                  gdouble      x,
                  gdouble      y)
{
  vector->x = x;
  vector->y = y;
}

/**
 * gimp_vector2_length:
 * @vector: a pointer to a #GimpVector2.
 *
 * Computes the length of a 2D vector.
 *
 * Returns: the length of @vector (a positive gdouble).
 **/
gdouble
gimp_vector2_length (const GimpVector2 *vector)
{
  return (sqrt (vector->x * vector->x + vector->y * vector->y));
}

/**
 * gimp_vector2_length_val:
 * @vector: a #GimpVector2.
 *
 * This function is identical to gimp_vector2_length() but the
 * vector is passed by value rather than by reference.
 *
 * Returns: the length of @vector (a positive gdouble).
 **/
gdouble
gimp_vector2_length_val (GimpVector2 vector)
{
  return (sqrt (vector.x * vector.x + vector.y * vector.y));
}

/**
 * gimp_vector2_mul:
 * @vector: a pointer to a #GimpVector2.
 * @factor: a scalar.
 *
 * Multiplies each component of the @vector by @factor. Note that this
 * is equivalent to multiplying the vectors length by @factor.
 **/
void
gimp_vector2_mul (GimpVector2 *vector,
                  gdouble      factor)
{
  vector->x *= factor;
  vector->y *= factor;
}

/**
 * gimp_vector2_mul_val:
 * @vector: a #GimpVector2.
 * @factor: a scalar.
 *
 * This function is identical to gimp_vector2_mul() but the vector is
 * passed by value rather than by reference.
 *
 * Returns: the resulting #GimpVector2.
 **/
GimpVector2
gimp_vector2_mul_val (GimpVector2 vector,
                      gdouble     factor)
{
  GimpVector2 result;

  result.x = vector.x * factor;
  result.y = vector.y * factor;

  return result;
}


/**
 * gimp_vector2_normalize:
 * @vector: a pointer to a #GimpVector2.
 *
 * Normalizes the @vector so the length of the @vector is 1.0. The nul
 * vector will not be changed.
 **/
void
gimp_vector2_normalize (GimpVector2 *vector)
{
  gdouble len;

  len = gimp_vector2_length (vector);

  if (len != 0.0)
    {
      len = 1.0 / len;
      vector->x *= len;
      vector->y *= len;
    }
  else
    {
      *vector = gimp_vector2_zero;
    }
}

/**
 * gimp_vector2_normalize_val:
 * @vector: a #GimpVector2.
 *
 * This function is identical to gimp_vector2_normalize() but the
 * vector is passed by value rather than by reference.
 *
 * Returns: a #GimpVector2 parallel to @vector, pointing in the same
 * direction but with a length of 1.0.
 **/
GimpVector2
gimp_vector2_normalize_val (GimpVector2 vector)
{
  GimpVector2 normalized;
  gdouble     len;

  len = gimp_vector2_length_val (vector);

  if (len != 0.0)
    {
      len = 1.0 / len;
      normalized.x = vector.x * len;
      normalized.y = vector.y * len;
      return normalized;
    }
  else
    {
      return gimp_vector2_zero;
    }
}

/**
 * gimp_vector2_neg:
 * @vector: a pointer to a #GimpVector2.
 *
 * Negates the @vector (i.e. negate all its coordinates).
 **/
void
gimp_vector2_neg (GimpVector2 *vector)
{
  vector->x *= -1.0;
  vector->y *= -1.0;
}

/**
 * gimp_vector2_neg_val:
 * @vector: a #GimpVector2.
 *
 * This function is identical to gimp_vector2_neg() but the vector
 * is passed by value rather than by reference.
 *
 * Returns: the negated #GimpVector2.
 **/
GimpVector2
gimp_vector2_neg_val (GimpVector2 vector)
{
  GimpVector2 result;

  result.x = vector.x * -1.0;
  result.y = vector.y * -1.0;

  return result;
}

/**
 * gimp_vector2_add:
 * @result: destination for the resulting #GimpVector2.
 * @vector1: a pointer to the first #GimpVector2.
 * @vector2: a pointer to the second #GimpVector2.
 *
 * Computes the sum of two 2D vectors. The resulting #GimpVector2 is
 * stored in @result.
 **/
void
gimp_vector2_add (GimpVector2       *result,
                  const GimpVector2 *vector1,
                  const GimpVector2 *vector2)
{
  result->x = vector1->x + vector2->x;
  result->y = vector1->y + vector2->y;
}

/**
 * gimp_vector2_add_val:
 * @vector1: the first #GimpVector2.
 * @vector2: the second #GimpVector2.
 *
 * This function is identical to gimp_vector2_add() but the vectors
 * are passed by value rather than by reference.
 *
 * Returns: the resulting #GimpVector2.
 **/
GimpVector2
gimp_vector2_add_val (GimpVector2 vector1,
                      GimpVector2 vector2)
{
  GimpVector2 result;

  result.x = vector1.x + vector2.x;
  result.y = vector1.y + vector2.y;

  return result;
}

/**
 * gimp_vector2_sub:
 * @result: the destination for the resulting #GimpVector2.
 * @vector1: a pointer to the first #GimpVector2.
 * @vector2: a pointer to the second #GimpVector2.
 *
 * Computes the difference of two 2D vectors (@vector1 minus @vector2).
 * The resulting #GimpVector2 is stored in @result.
 **/
void
gimp_vector2_sub (GimpVector2       *result,
                  const GimpVector2 *vector1,
                  const GimpVector2 *vector2)
{
  result->x = vector1->x - vector2->x;
  result->y = vector1->y - vector2->y;
}

/**
 * gimp_vector2_sub_val:
 * @vector1: the first #GimpVector2.
 * @vector2: the second #GimpVector2.
 *
 * This function is identical to gimp_vector2_sub() but the vectors
 * are passed by value rather than by reference.
 *
 * Returns: the resulting #GimpVector2.
 **/
GimpVector2
gimp_vector2_sub_val (GimpVector2 vector1,
                      GimpVector2 vector2)
{
  GimpVector2 result;

  result.x = vector1.x - vector2.x;
  result.y = vector1.y - vector2.y;

  return result;
}

/**
 * gimp_vector2_inner_product:
 * @vector1: a pointer to the first #GimpVector2.
 * @vector2: a pointer to the second #GimpVector2.
 *
 * Computes the inner (dot) product of two 2D vectors.
 * This product is zero if and only if the two vectors are orthognal.
 *
 * Returns: The inner product.
 **/
gdouble
gimp_vector2_inner_product (const GimpVector2 *vector1,
                            const GimpVector2 *vector2)
{
  return (vector1->x * vector2->x + vector1->y * vector2->y);
}

/**
 * gimp_vector2_inner_product_val:
 * @vector1: the first #GimpVector2.
 * @vector2: the second #GimpVector2.
 *
 * This function is identical to gimp_vector2_inner_product() but the
 * vectors are passed by value rather than by reference.
 *
 * Returns: The inner product.
 **/
gdouble
gimp_vector2_inner_product_val (GimpVector2 vector1,
                                GimpVector2 vector2)
{
  return (vector1.x * vector2.x + vector1.y * vector2.y);
}

/**
 * gimp_vector2_cross_product:
 * @vector1: a pointer to the first #GimpVector2.
 * @vector2: a pointer to the second #GimpVector2.
 *
 * Compute the cross product of two vectors. The result is a
 * #GimpVector2 which is orthognal to both @vector1 and @vector2. If
 * @vector1 and @vector2 are parallel, the result will be the nul
 * vector.
 *
 * Note that in 2D, this function is useful to test if two vectors are
 * parallel or not, or to compute the area spawned by two vectors.
 *
 * Returns: The cross product.
 **/
GimpVector2
gimp_vector2_cross_product (const GimpVector2 *vector1,
                            const GimpVector2 *vector2)
{
  GimpVector2 normal;

  normal.x = vector1->x * vector2->y - vector1->y * vector2->x;
  normal.y = vector1->y * vector2->x - vector1->x * vector2->y;

  return normal;
}

/**
 * gimp_vector2_cross_product_val:
 * @vector1: the first #GimpVector2.
 * @vector2: the second #GimpVector2.
 *
 * This function is identical to gimp_vector2_cross_product() but the
 * vectors are passed by value rather than by reference.
 *
 * Returns: The cross product.
 **/
GimpVector2
gimp_vector2_cross_product_val (GimpVector2 vector1,
                                GimpVector2 vector2)
{
  GimpVector2 normal;

  normal.x = vector1.x * vector2.y - vector1.y * vector2.x;
  normal.y = vector1.y * vector2.x - vector1.x * vector2.y;

  return normal;
}

/**
 * gimp_vector2_rotate:
 * @vector: a pointer to a #GimpVector2.
 * @alpha: an angle (in radians).
 *
 * Rotates the @vector counterclockwise by @alpha radians.
 **/
void
gimp_vector2_rotate (GimpVector2 *vector,
                     gdouble      alpha)
{
  GimpVector2 result;

  result.x = cos (alpha) * vector->x + sin (alpha) * vector->y;
  result.y = cos (alpha) * vector->y - sin (alpha) * vector->x;

  *vector = result;
}

/**
 * gimp_vector2_rotate_val:
 * @vector: a #GimpVector2.
 * @alpha: an angle (in radians).
 *
 * This function is identical to gimp_vector2_rotate() but the vector
 * is passed by value rather than by reference.
 *
 * Returns: a #GimpVector2 representing @vector rotated by @alpha
 * radians.
 **/
GimpVector2
gimp_vector2_rotate_val (GimpVector2 vector,
                         gdouble     alpha)
{
  GimpVector2 result;

  result.x = cos (alpha) * vector.x + sin (alpha) * vector.y;
  result.y = cos (alpha) * vector.y - sin (alpha) * vector.x;

  return result;
}

/**
 * gimp_vector2_normal:
 * @vector: a pointer to a #GimpVector2.
 *
 * Compute a normalized perpendicular vector to @vector
 *
 * Returns: a #GimpVector2 perpendicular to @vector, with a length of 1.0.
 *
 * Since: 2.8
 **/
GimpVector2
gimp_vector2_normal (GimpVector2 *vector)
{
  GimpVector2 result;

  result.x = - vector->y;
  result.y = vector->x;

  gimp_vector2_normalize (&result);

  return result;
}

/**
 * gimp_vector2_normal_val:
 * @vector: a #GimpVector2.
 *
 * This function is identical to gimp_vector2_normal() but the vector
 * is passed by value rather than by reference.
 *
 * Returns: a #GimpVector2 perpendicular to @vector, with a length of 1.0.
 *
 * Since: 2.8
 **/
GimpVector2
gimp_vector2_normal_val (GimpVector2 vector)
{
  GimpVector2 result;

  result.x = - vector.y;
  result.y = vector.x;

  gimp_vector2_normalize (&result);

  return result;
}
