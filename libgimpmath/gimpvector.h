/* LIBGIMP - The GIMP Library
 * Copyright (C) 1995-1997 Peter Mattis and Spencer Kimball
 *
 * gimpvector.h
 *
 * The gimp_vector* functions were taken from:
 * GCK - The General Convenience Kit
 * Copyright (C) 1996 Tom Bech
 *
 * This library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see
 * <https://www.gnu.org/licenses/>.
 */

#ifndef __GIMP_VECTOR_H__
#define __GIMP_VECTOR_H__

G_BEGIN_DECLS

/* For information look into the C source or the html documentation */

/* TEMP: as defined in libgimpcolor/gimpcolortypes.h */
typedef struct _GimpRGB  GimpRGB;
struct _GimpRGB
{
  gdouble r, g, b, a;
};

/* TEMP: as defined in app/core/core-types.h */
#define MAX_CHANNELS  4

/* TEMP: just for the sake of getting rid of assert calls.
 * Anyway this whole file will disappear when moved into GIMP. */
#include <assert.h>
#define gimp_assert assert

typedef struct _GimpVector2 GimpVector2;

/**
 * GimpVector2:
 * @x: the x axis
 * @y: the y axis
 *
 * A two dimensional vector.
 **/
struct _GimpVector2
{
  gdouble x, y;
};


/* Two dimensional vector functions */
/* ================================ */

GimpVector2 gimp_vector2_new               (gdouble            x,
                                            gdouble            y);
void        gimp_vector2_set               (GimpVector2       *vector,
                                            gdouble            x,
                                            gdouble            y);
gdouble     gimp_vector2_length            (const GimpVector2 *vector);
gdouble     gimp_vector2_length_val        (GimpVector2        vector);
void        gimp_vector2_mul               (GimpVector2       *vector,
                                            gdouble            factor);
GimpVector2 gimp_vector2_mul_val           (GimpVector2        vector,
                                            gdouble            factor);
void        gimp_vector2_normalize         (GimpVector2       *vector);
GimpVector2 gimp_vector2_normalize_val     (GimpVector2        vector);
void        gimp_vector2_neg               (GimpVector2       *vector);
GimpVector2 gimp_vector2_neg_val           (GimpVector2        vector);
void        gimp_vector2_add               (GimpVector2       *result,
                                            const GimpVector2 *vector1,
                                            const GimpVector2 *vector2);
GimpVector2 gimp_vector2_add_val           (GimpVector2        vector1,
                                            GimpVector2        vector2);
void        gimp_vector2_sub               (GimpVector2       *result,
                                            const GimpVector2 *vector1,
                                            const GimpVector2 *vector2);
GimpVector2 gimp_vector2_sub_val           (GimpVector2        vector1,
                                            GimpVector2        vector2);
gdouble     gimp_vector2_inner_product     (const GimpVector2 *vector1,
                                            const GimpVector2 *vector2);
gdouble     gimp_vector2_inner_product_val (GimpVector2        vector1,
                                            GimpVector2        vector2);
GimpVector2 gimp_vector2_cross_product     (const GimpVector2 *vector1,
                                            const GimpVector2 *vector2);
GimpVector2 gimp_vector2_cross_product_val (GimpVector2        vector1,
                                            GimpVector2        vector2);
void        gimp_vector2_rotate            (GimpVector2       *vector,
                                            gdouble            alpha);
GimpVector2 gimp_vector2_rotate_val        (GimpVector2        vector,
                                            gdouble            alpha);
GimpVector2 gimp_vector2_normal            (GimpVector2       *vector);
GimpVector2 gimp_vector2_normal_val        (GimpVector2        vector);

G_END_DECLS

#endif  /* __GIMP_VECTOR_H__ */
