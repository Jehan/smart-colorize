                   +-----------------------------------+
                   | Smart-Colorize C++ implementation |
                   +-----------------------------------+

Authors
-------

Sébastien Fourey & David Tschumperlé
GREYC
CNRS, Ensicaen, University of Caen
http://www.greyc.fr

Compilation
-----------

  Run the "build.sh" script, preferably, to compile in release mode (and enable -Ofast).

Execution / Testing
-------------------

 * Quick test

   Simply run ./smart-colorize after compilation.

   By default, crop_inktober.jpg (lineart) and crop_inktober_spots.png (color
   spots) from the images/ folder will be used as input images. The "Fill
   from color spots" method is then used, and the result will be displayed.

 * Test with custom input images

   The program may be used in two modes :

  - First close the lineart, then create a layer of regions with random colors.
    In this mode, a single input file is required (the lineart).  The -r option
    is used to name the output image of colored regions. The -o option sets the name of
    the merged image (lineart + colors).

     ./smart-colorize -i lineart.png -r output_regions.png -o output_image.png

    or

     ./smart-colorize -i lineart.png                               # Result is displayed


  - First close the lineart, then create an image of regions from a given image
    of "color spots". In this mode, two input files are provided (lineart & color spots).

     ./smart-colorize -i lineart.png -c color_spots.png \
                      -r output_regions.png -o output_image.png

    or

     ./smart-colorize -i lineart.png -c color_spots.png            # Result is displayed

Sample commands
---------------

# Same result as "./smart-colorize" (with default inputs as lineart and color spots)
./smart-colorize -i images/crop_inktober.jpg -c images/crop_inktober_spots.png

# Use AutoFill (no color spots provided)
./smart-colorize -i images/crop_inktober.jpg

# Use gray level spots
./smart-colorize -i images/crop_inktober.jpg -c images/crop_inktober_gray_spots.png

Credits
-------

 * Images crop_inktober are courtesy of David Revoy.

 * The CImg C++ library (by David Tschumperlé) is used and shipped with this
   source code.

License
-------

See COPYING file (GPLv3).
